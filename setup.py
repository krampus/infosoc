"""Information Society: a federated imageboard platform"""

from setuptools import setup

authors = {
    'krampus': 'contact@robkel.ly'
}

extras = dict(
    lint=[
        'autopep8',
        'flake8',
    ],
    develop=[
        'autopep8',
        'bpython',
        'flake8',
        'flask-shell-bpython',
        'jedi',
        'rope',
        'setuptools_scm',
    ],
    test=[
        'coverage',
        'nose',
        'nose-timer',
        'rednose',
    ]
)

extras['complete'] = list({pkg for req in extras.values() for pkg in req})

setup(
    name='infosoc',
    description=__doc__,
    author=', '.join(authors.keys()),
    author_email=', '.join(authors.values()),
    keywords="infosoc information society imageboard booru federated federation activitypub",
    url="https://gitlab.com/krampus/infosoc/",
    project_urls={
        'Issue Tracker': "https://gitlab.com/krampus/infosoc/issues",
        'Releases': "https://gitlab.com/krampus/infosoc/releases",
    },
    packages=[
        'infosoc',
    ],
    use_scm_version=True,
    install_requires=[
        'ansicolors==1.1.8',
        'bleach==3.1.0',
        'Flask==1.1.1',
        'Flask-Migrate==2.5.2',
        'Flask-Misaka==1.0.0',
        'Flask-SQLAlchemy==2.4.0',
        'Pillow==6.1.0',
        'pyparsing==2.4.1.1',
        'PyYAML==5.1.1',
        'requests==2.22.0',
        'SQLAlchemy==1.3.6',
    ],
    setup_requires=[
        'setuptools_scm',
    ],
    tests_require=extras['test'],
    extras_require=extras,
    test_suite='nose.collector',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Framework :: Flask',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Topic :: Communications :: File Sharing',
    ]
)
