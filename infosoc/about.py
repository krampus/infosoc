"""About page view"""
from flask import Blueprint, render_template

from .db.models import User, Post, Tag
from .app import instance_file
from . import __version__

bp = Blueprint('about', __name__, url_prefix='/about')


@bp.route('/')
def view():
    return render_template('about/index.html', info=dict(
        package=__package__,
        version=__version__,
        n_users=User.query.count(),
        n_posts=Post.query.count(),
        n_tags=Tag.query.count()
    ))


@bp.route('/queries')
def queries():
    from .query import parser
    return render_template('about/queries.html', sections=[
        parser.Tag,
        parser.AndOp,
        parser.OrOp,
        parser.XorOp,
        parser.NotOp,
        parser.NamedTag,
        parser.MetaTag,
    ])


@bp.route('/tos')
def terms_of_service():
    tos_path = instance_file('terms_of_service.md')
    with open(tos_path, 'r') as f:
        tos_markdown = f.read()
    return render_template('about/tos.html', content=tos_markdown)
