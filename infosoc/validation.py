"""Data validation utilities"""
import re
from email.utils import parseaddr
import bleach

from .db.models import User
from .db.mimetypes import ImageTypes, VideoTypes
from .config import config

_password_pattern = re.compile(config.validation.password_pattern)
_nickname_pattern = re.compile(config.validation.nickname_pattern)
_tag_pattern = re.compile(config.validation.tag_pattern)


def _has_value(string):
    return string is not None and len(string) > 0


def validate_nickname(nickname):
    """Assert that the given nickname meets requirements and is not taken."""
    assert _has_value(nickname), 'missing nickname'

    existing = User.query.filter_by(nickname=nickname).first()
    assert existing is None, f'nickname {nickname} is already taken'

    assert _nickname_pattern.fullmatch(nickname), \
        f'invalid nickname (must match /{config.validation.nickname_pattern}/)'

    assert nickname == bleach.clean(nickname), \
        f"invalid nickname (did not pass bleaching)"

    return nickname


def validate_password(password):
    """Assert that the given password meets requirements (whatever that means)."""
    assert _has_value(password), 'missing password'
    assert _password_pattern.fullmatch(password), \
        f'invalid password (must match /{config.validation.password_pattern}/)'
    return password


def validate_email(email):
    """Assert that the given email is real, meets requirements, and is not already in use."""
    if config.instance.email_required:
        assert _has_value(email), 'missing email address'
    elif not _has_value(email):
        return

    # validate address
    _, parsed_email = parseaddr(email)
    assert '@' in parsed_email, 'invalid email address'
    assert email == parsed_email, 'malformed email address'

    # TODO better email checking
    return email


def validate_text(text):
    """Assert that the given generic text input meets requirements"""
    return bleach.clean(text) if _has_value(text) else None


def validate_tag(tag):
    """Assert that the given string meets the requirements for a tag"""
    if _has_value(tag):
        tag = bleach.clean(tag)
        assert _tag_pattern.fullmatch(tag), \
            f'invalid tag {tag} (must match /{config.validation.tag_pattern}/)'

        # Instances may allow ':' in tags, in which case we need to
        # make sure there's no conflict with query meta-tags
        head, *tail = tag.split(':')
        if len(tail) > 0:
            from .query.parser import KVTag
            from .query import special_tags  # noqa F401
            special_keys = [t.key for sub in KVTag.__subclasses__() for t in sub.__subclasses__()]
            assert head not in special_keys, f'tags cannot start with `{head}:`'

        return tag


def _validate_file_store(file_store, allowed_types):
    if not _has_value(file_store.filename):
        # user did not select a file to upload
        return
    else:
        mime_type = file_store.headers['Content-Type']
        assert mime_type in allowed_types, f"media type `{mime_type}` is not allowed"
        # max content length is handled automatically by Flask
        return file_store


def validate_media(file_store):
    """Assert that the media spooled in the given file storage container meets requirements for file uploads"""
    return _validate_file_store(file_store, config.media.allowed_types)


def validate_image(file_store):
    """Assert that the media spooled in the given file storage container is an image of allowed type"""
    allowed_types = [t.value for t in ImageTypes if t.value in config.media.allowed_types]
    return _validate_file_store(file_store, allowed_types)


def validate_video(file_store):
    """Assert that the media spooled in the given file storage container is a video of allowed type"""
    allowed_types = [t.value for t in VideoTypes if t.value in config.media.allowed_types]
    return _validate_file_store(file_store, allowed_types)
