"""Post view"""
import functools

from flask import Blueprint, render_template, redirect, flash, url_for, request, g, current_app

from . import media, notification
from .app import db, alert_on_exception
from .db.models import Post, Tag
from .auth import login_required
from .validation import validate_text, validate_tag, validate_media
from .config import config

bp = Blueprint('post', __name__, url_prefix='/post')


def valid_post_required(fn):
    @functools.wraps(fn)
    def inner(**kwargs):
        post = Post.query.filter_by(**kwargs).first()
        if post is not None:
            return fn(post)
        else:
            flash(f"no such post with {kwargs} on this instance", category='alert-danger')
            return redirect(url_for('index'))
    return inner


@bp.route('<id>', methods=('GET', 'POST'))
@valid_post_required
@alert_on_exception()
def view(post):
    if request.method == 'POST':
        warnings = []
        # removed tags
        for name in request.form.getlist('remove[]'):
            tag = Tag.query.filter_by(name=name).first()
            if tag is not None:
                if tag in post.tags:
                    post.tags.remove(tag)
                else:
                    warnings.append(f'post #{post.id} is not tagged with `{name}`')
            else:
                warnings.append(f'no such tag `{name}`')

        # new tags
        unique_tags = set(request.form['newtags'].split())
        valid_tags = [validate_tag(tag) for tag in unique_tags]
        for name in valid_tags:
            tag = Tag.query.filter_by(name=name).first() or Tag(name=name)
            if tag not in post.tags:
                current_app.logger.info(f"new tag `{name}` for {post}")
                post.tags.append(tag)
            else:
                warnings.append(f'duplicate tag `{name}` for post #{post.id}')

        db.session.commit()
        # TODO figure out how we're federating tag updates
        # TODO federate
        for warning in warnings:
            flash(warning, category='alert-warning')
        flash(f'updated tags for post #{post.id}', category='alert-success')

        return redirect(url_for('post.view', id=post.id))
    else:
        if request.args.get('edit_tags', None):
            # TODO check if user meets requirements to edit tags
            return render_template('post/tag_editor.html', post=post)
        else:
            return render_template('post/view.html', post=post)


@bp.route('<id>/like', methods=['POST'])
@login_required
@valid_post_required
def like(post):
    if post in g.user.liked_posts:
        g.user.liked_posts.remove(post)
    else:
        g.user.liked_posts.append(post)
    db.session.commit()

    if post.author != g.user:
        notification.on_like(post, g.user)

    # refresh page
    return redirect(request.environ['HTTP_REFERER'])


@bp.route('<id>/reply')
@login_required
@valid_post_required
def reply(post):
    return render_template('post/reply.html', post=post)


@bp.route('<id>/delete', methods=('GET', 'POST'))
@valid_post_required
@alert_on_exception(category=AssertionError)
def delete(post):
    if g.user and (g.user.id == post.author.id or g.user.is_moderator):
        if request.method == 'POST':
            pid = post.id
            db.session.delete(post)
            db.session.commit()
            # TODO federate
            flash(f"deleted post #{pid}", category='alert-success')
            return redirect(url_for('index'))
        else:
            return render_template('post/delete.html', post=post)
    else:
        flash(f"only {post.author.handle} can do that", category='alert-danger')
        return redirect(url_for('post.view', id=post.id))


@bp.route('/', methods=['POST'])
@login_required
@alert_on_exception()
def submit():
    description = validate_text(request.form.get('description', None))
    subject = validate_text(request.form.get('subject', None))
    uploads = [media.prepare_upload(f) for f in request.files.getlist('media') if validate_media(f) is not None]

    if config.media.allow_uploads_from_url:
        upload_urls = request.form.get('mediaURLs', '').split()
        for url in upload_urls:
            request_store = media.RequestFromURL(url)
            if validate_media(request_store):
                uploads.append(media.prepare_upload(request_store))

    reply_to_id = request.form.get('in_reply_to', None)
    reply_to_post = Post.query.get(reply_to_id) if reply_to_id is not None else None

    unique_tags = set(request.form.get('tags', '').split())
    valid_tags = [validate_tag(tag) for tag in unique_tags]

    # use first image file for thumbnail
    thumbnail = None
    for prep in uploads:
        thumbnail_prep = prep.thumbnail()
        if thumbnail_prep is not None:
            thumbnail = thumbnail_prep.insert()
            current_app.logger.info(f"Found image for thumbnail: {thumbnail.filename}")
            break

    post = Post(
        author=g.user,
        thumbnail=thumbnail,
        description=description,
        subject=subject,
        media=[prep.insert() for prep in uploads],
        tags=[Tag.query.filter_by(name=tag).first() or Tag(name=tag) for tag in valid_tags],
        reply_to=reply_to_post
    )

    db.session.add(post)
    db.session.commit()

    if reply_to_post is not None and reply_to_post.author != g.user:
        notification.on_reply(post, reply_to_post)

    # TODO federate
    return redirect(url_for('post.view', id=post.id))
