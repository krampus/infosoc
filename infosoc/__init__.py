"""Information Society (infosoc) -- A federated imageboard"""

################################################################################
# Information Society (infosoc) -- A federated imageboard
# Copyright (C) 2019  Rob Kelly
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# dynamic package versioning with setuptools_scm
try:
    from pkg_resources import get_distribution, DistributionNotFound
    __version__ = get_distribution(__package__).version
except DistributionNotFound:
    # package not installed
    try:
        from pathlib import Path
        from setuptools_scm import get_version
        root_path = Path(__file__).parent.parent.absolute()
        __version__ = get_version(root=root_path)
    except Exception:
        __version__ = 'unknown'

import logging
from logging.config import dictConfig
from colors import color


class ColorFormatter(logging.Formatter):
    _level_colors = {
        logging.WARNING: {'fg': 'yellow'},
        logging.ERROR: {'fg': 'red'},
        logging.CRITICAL: {'fg': 'red', 'bg': 'yellow'},
    }
    _default_level_color = {'fg': 'cyan'}

    def format(self, record):
        if record.module == '_internal':
            header_fmt = '[%(asctime)s] %(levelname)s (wsgi)'
        else:
            header_fmt = '[%(asctime)s] %(levelname)s in %(module)s.%(funcName)s:L%(lineno)s'

        kwargs = self._level_colors.get(record.levelno, self._default_level_color)
        log_fmt = f"{color(header_fmt, **kwargs)}: %(message)s"
        self._style = logging._STYLES['%'][0](log_fmt)
        return super().format(record)


dictConfig(dict(
    version=1,
    formatters=dict(
        default={
            '()': ColorFormatter,
        }
    ),
    handlers=dict(
        wsgi={
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        }
    ),
    root=dict(
        level='INFO',
        handlers=['wsgi']
    )
))

from .app import app  # noqa: F401
