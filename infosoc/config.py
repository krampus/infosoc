"""Custom configuration loading tools"""
import os
from yaml import load as yaml_load
import functools
from urllib.parse import urlparse
from pathlib import Path
from types import SimpleNamespace

try:
    from yaml import CLoader as YamlLoader
except ImportError:
    from yaml import Loader as YamlLoader

# Singleton instance to be populated by init methods in this module
config = SimpleNamespace(
    __initialized__=False
)


def _after_init(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        assert config.__initialized__, "Configuration not initialized!"
        return fn(*args, **kwargs)
    return wrapper


def initialize(**kwargs):
    def normalize(e):
        if isinstance(e, dict):
            return SimpleNamespace(**{k: normalize(v) for k, v in e.items()})
        else:
            return e

    for k, v in kwargs.items():
        setattr(config, k, normalize(v))

    config.instance.url = urlparse(config.instance.url)

    alias_map = {}
    canon_names = {}
    for canon_tag, aliases in config.instance.aliases.__dict__.items():
        all_aliases = set(aliases + [canon_tag])
        alias_map[canon_tag] = all_aliases

        for tag in aliases:
            alias_map[tag] = all_aliases
            canon_names[tag] = canon_tag

    config.instance.get_aliases = lambda tag: alias_map.get(tag, {tag})
    config.instance.canon_name = lambda tag: canon_names.get(tag, tag)
    config.instance.is_canon = lambda tag: tag not in canon_names

    config.__initialized__ = True


def init_from_yaml(yaml_path):
    with open(yaml_path, 'r') as f:
        conf_dict = yaml_load(f, Loader=YamlLoader)
    initialize(**conf_dict)


@_after_init
def init_app(app):
    instance_path = Path(app.instance_path)

    app.config['SECRET_KEY'] = config.application.secret_key
    config.application.secret_key = None

    if config.application.proxy_path:
        from .reverse_proxy import ReverseProxied
        app.wsgi_app = ReverseProxied(app.wsgi_app,
                                      script_name=config.application.proxy_path)

    # build media path and make sure it exists
    media_path = Path(config.media.store_path)
    if not media_path.is_absolute():
        media_path = instance_path / media_path
    media_path.mkdir(exist_ok=True, parents=True)

    app.config['MEDIA_PATH'] = media_path
    app.config['SQLALCHEMY_DATABASE_URI'] = config.database.uri.format(instance_path=instance_path)
    app.config['SQLALCHEMY_ECHO'] = config.database.echo

    app.config['MAX_CONTENT_LENGTH'] = config.media.max_size

    app.jinja_env.globals['site_config'] = config

    def fmtdate(dt, format=config.instance.date_format):
        return dt.strftime(format)

    app.jinja_env.filters['fmtdate'] = fmtdate


def load(app):
    instance_path = Path(app.instance_path)

    site_conf_path = Path(os.environ.get('CONFIG_YAML', instance_path / 'config.yaml'))

    try:
        init_from_yaml(site_conf_path)
    except Exception as site_ex:
        app.logger.warn(f"Couldn't read site config from {site_conf_path}: {site_ex}")
        if isinstance(site_ex, FileNotFoundError):
            app.logger.warn("(hint: copy from `default` and edit as needed)")
        default_conf_path = instance_path.parent / 'default' / 'config.yaml'
        app.logger.warn(f"Falling back to default config from {default_conf_path}...")
        try:
            init_from_yaml(default_conf_path)
        except Exception as default_ex:
            app.logger.critical(F"Couldn't read default config from {default_conf_path}: {default_ex}")

    init_app(app)
