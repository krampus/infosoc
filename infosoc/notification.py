"""Views relating to notifications"""
import functools
from flask import Blueprint, redirect, url_for, flash, request

from .db.models import Notification
from .auth import login_required
from .app import db

bp = Blueprint('notification', __name__, url_prefix='/notification')


def valid_notification_required(fn):
    @functools.wraps(fn)
    def inner(**kwargs):
        note = Notification.query.filter_by(**kwargs).first()
        if note is not None:
            return fn(note)
        else:
            flash(f"no such notification with {kwargs} on this instance", category='alert-danger')
            return redirect(url_for('index'))
    return inner


def on_reply(reply_post, reply_to_post):
    """Notify the author of a post that a user has replied to their post"""
    db.session.add(Notification(
        category='alert-success',
        content=f'{reply_post.author.handle} has replied to your post',
        href=url_for('post.view', id=reply_post.id),
        user=reply_to_post.author
    ))
    db.session.commit()


def on_like(liked_post, user):
    """Notify the author of a post that a user has liked their post"""
    db.session.add(Notification(
        category='alert-warning',
        content=f'{user.handle} has favorited your post',
        href=url_for('post.view', id=liked_post.id),
        user=liked_post.author
    ))
    db.session.commit()


def on_mod(user):
    """Notify a user that they have been promoted to instance moderator"""
    db.session.add(Notification(
        category='alert-danger',
        content=f'you have been made a moderator of this instance',
        user=user
    ))
    db.session.commit()


def on_demod(user):
    """Notify a user that they have been demoted from instance moderator"""
    db.session.add(Notification(
        category='alert-danger',
        content=f'you have been removed as a moderator of this instance',
        user=user
    ))
    db.session.commit()


@bp.route('/<id>/dismiss', methods=['POST'])
@login_required
@valid_notification_required
def dismiss(note):
    db.session.delete(note)
    db.session.commit()
    return redirect(request.form.get('redirectURL', request.environ['HTTP_REFERER']))
