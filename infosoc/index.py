"""Index view"""
import functools
from flask import Blueprint, render_template, request

from . import query
from .config import config

bp = Blueprint('index', __name__, url_prefix='/posts')


def paginated(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        page_num = int(request.args.get('page', 1))
        return fn(*args, **kwargs, pager=lambda q: q.paginate(page=page_num, per_page=config.ui.results_per_page))
    return wrapper


@bp.route('/')
@paginated
def results(pager):
    query_string = request.args.get('query', '')

    # TODO cache
    qb = query.posts(query_string)

    page = pager(qb.build())
    return render_template(
        'index.html',
        query=query_string,
        page=page,
        description=str(qb)
    )
