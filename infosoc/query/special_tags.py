"""Parser tokens with special functions

Important note for developers: certain members of special tags are
used to build the tag query user guide on the `about/queries'
page. Markdown formatting is available.

- Docstrings are used as the tag description, and should serve as a
  general usage guide.
- `__about_name__` is used as the section header.
- `__about_values__` may either be a string describing allowed values,
  or a dict mapping each allowed value to a markdown-formatted
  description.
- `__about_examples__` is a map of example queries to a
  markdown-formatted description of the returned results.

Any of these fields may be omitted, but users are more likely to use
your feature when they know what the hell it does.
"""
import pyparsing as pp
from .parser import NamedTag, MetaTag, ComparisonToken
from ..db.models import Post, Media
from ..db import mimetypes
from ..config import config


class SortTag(MetaTag):
    """the `sort` meta-tag changes how results are ordered."""
    key = 'sort'
    orderings = dict(
        # time of posting
        date_asc=(Post.inserted_at.asc(), "order by date posted, oldest to newest"),
        date_desc=(Post.inserted_at.desc(), "order by date posted, newest to oldest"),
        date=(Post.inserted_at.desc(), "shortcut for `date_desc`"),
        # time of last update
        edited_asc=(Post.updated_at.asc(), "order by date last edited, oldest to newest"),
        edited_desc=(Post.updated_at.desc(), "order by date last edited, newest to oldest"),
        edited=(Post.updated_at.desc(), "shortcut for `edited_desc`"),
        # number of tags
        tags_asc=(Post.tag_count.asc(), "order by number of tags, fewest to most"),
        tags_desc=(Post.tag_count.desc(), "order by number of tags, most to fewest"),
        tags=(Post.tag_count.desc(), "shortcut for `tags_desc`"),
        # number of likes
        likes_asc=(Post.like_count.asc(), "order by number of favorites, fewest to most"),
        likes_desc=(Post.like_count.desc(), "order by number of favorites, most to fewest"),
        likes=(Post.like_count.desc(), "shortcut for `likes_desc`"),
        # number of replies
        replies_asc=(Post.reply_count.asc(), "order by number of replies, fewest to most"),
        replies_desc=(Post.reply_count.desc(), "order by number of replies, most to fewest"),
        replies=(Post.reply_count.desc(), "shortcut for `replies_desc`"),
    )
    values = list(orderings.keys())
    __about_name__ = "`sort`"
    __about_values__ = {k: v[1] for k, v in orderings.items()}
    __about_examples__ = {
        'sort:likes': "all posts, sorted from most-favorited to least",
        'cool dog sort:date_asc': "posts tagged with `cool` and `dog`, sorted by date from earliest to latest"
    }

    def filter_query(self, qb):
        qb.order_by = self.orderings.get(self.value, self.orderings['date_desc'])[0]

    def __str__(self):
        return f"sorting by {self.value}"


class AuthorTag(NamedTag):
    """the `author` tag specifies posts made by the given user."""
    key = 'author'
    __about_name__ = "`author`"
    __about_examples__ = {
        'author:guy': "all posts posted by user @guy",
        'cool_post author:guy whatever': "posts by user @guy tagged with `cool_post` and `whatever`"
    }
    __about_values__ = "the nickname of any user known to this instance"

    @classmethod
    def _value_parser(cls):
        return pp.Regex(config.validation.nickname_pattern)

    def build_query(self):
        return Post.author.has(nickname=self.value)


class SubjectTag(NamedTag):
    """the `subject` tag specifies posts containing the given in the subject field.

    the string given as a value may appear anywhere in the subject,
    and is case-insensitive.
    """
    key = 'subject'
    __about_name__ = "`subject`"
    __about_examples__ = {
        'subject:"the spectacle"': 'posts containing the phrase "the spectacle" anywhere in the subject',
        'cool_post, subject:"cool"': ('posts either tagged with `cool post` or containing '
                                      'the phrase "cool" anywhere in the subject (or both)')
    }
    __about_values__ = "any string surrounded by double- or single-quotes (`\"` or `'` respectively)"

    @classmethod
    def _value_parser(cls):
        return pp.QuotedString("'") | pp.QuotedString('"')

    def build_query(self):
        return Post.subject.ilike(f'%{self.value}%')


class DescriptionTag(NamedTag):
    """the `description` tag specifies posts containing the given string in the body of the post.

    the string given as a value may appear anywhere in the
    description, and is case-insensitive.
    """
    key = 'description'
    __about_name__ = "`description`"
    __about_examples__ = {
        'description:"the spectacle"': 'posts containing the phrase "the spectacle" anywhere in the body of the post',
        'description:"They Live", they_live': ('posts either tagged with `they_live` or containing the '
                                               'phrase "They Live" anywhere in the body of the post (or both)')
    }
    __about_values__ = "any string surrounded by double- or single-quotes (`\"` or `'` respectively)"

    @classmethod
    def _value_parser(cls):
        return pp.QuotedString("'") | pp.QuotedString('"')

    def build_query(self):
        return Post.description.ilike(f'%{self.value}%')


class MediaCategoryTag(NamedTag):
    """the `media` tag can be used to specify posts containing media of a given category."""
    key = 'media'
    media_categories = dict(
        image=(mimetypes.ImageTypes, "posts with image media"),
        video=(mimetypes.VideoTypes, "posts with video media"),
        audio=(mimetypes.AudioTypes, "posts with audio media"),
        binary=(mimetypes.BinaryTypes, "posts with other binary-format media"),
        document=(mimetypes.DocumentTypes, "posts with document media"),
        multimedia=(mimetypes.MultimediaTypes, "posts with multimedia content, like Flash"),
        none=(None, "posts without any attached media"),
    )
    __about_name__ = "`media` (by category)"
    __about_values__ = {k: v[1] for k, v in media_categories.items()}
    __about_examples__ = {
        "vaporwave (media:audio, media:image)": ("posts tagged with `vaporwave` and with either "
                                                 "audio or image media (or both)")
    }

    @classmethod
    def _value_parser(cls):
        return pp.oneOf(cls.media_categories.keys())

    def build_query(self):
        category = self.media_categories[self.value][0]
        if category is None:
            return Post.media == None  # noqa: E711
        else:
            return Post.media.any(Media.mime_type.in_([t.name for t in category]))


class MediaMIMETypeTag(NamedTag):
    """the `media` tag can also be used to specify posts containing media of a given MIME type."""
    key = 'media'
    __about_name__ = "`media` (by MIME type)"
    __about_examples__ = {
        'media:image/gif': "posts with GIF (.gif) media",
        'media:video/mp4': "posts with MPEG-4 (.mp4) video media (which can be played on iOS)",
        '(media:text/plain, media:text/markdown) story': ("posts tagged with `story` and with either "
                                                          "plaintext (.txt) or markdown (.md) media (or both)")
    }

    # TODO link to mime type instance documentation
    __about_values__ = f"any supported MIME type"

    @classmethod
    def _value_parser(cls):
        return pp.oneOf([t.value for t in mimetypes.MIMEType])

    def build_query(self):
        mt = mimetypes.MIMEType(self.value)
        return Post.media.any(Media.mime_type == mt)


class ReplyTag(NamedTag):
    """the `reply` tag specifies posts made in reply to a given post id (or not)."""
    __about_name__ = "`reply`"
    key = 'reply'
    __about_examples__ = {
        'reply:false': "posts which are not replies to any other post",
        'reply:420': "posts made in reply to post #420"
    }
    __about_values__ = "the ID number of a post on this instance, or `false`/`none` for posts which are not replies"
    _none_values = ['false', 'none', 'null', 'no', 'not']

    @classmethod
    def _value_parser(cls):
        return pp.Or([pp.pyparsing_common.integer, pp.oneOf(cls._none_values)])

    def build_query(self):
        if self.value in self._none_values:
            return Post.reply_to_id == None  # noqa: E711
        else:
            # The "!= None" check here seems redundant, but is required for the negation to function
            return (Post.reply_to_id != None) & (Post.reply_to_id == self.value)  # noqa: E711


class LikeCountTag(NamedTag):
    """the `likes` tag specifies posts with some given amount of likes."""
    __about_name__ = "`likes`"
    key = 'likes'
    __about_examples__ = {
        'likes:=0': "posts with exactly zero likes :(",
        'likes:>10': "posts with more than ten likes",
    }
    __about_values__ = f"a comparator ({ComparisonToken.__about_values__}) followed by a number"

    @classmethod
    def _value_parser(cls):
        return ComparisonToken._parser(pp.pyparsing_common.integer)

    def build_query(self):
        return self.value.compare(Post.like_count)


class ReplyCountTag(NamedTag):
    """the `replies` tag specifies posts with some given amount of replies."""
    __about_name__ = "`replies`"
    key = 'replies'
    __about_examples__ = {
        'replies:=0': "posts with exactly zero replies",
        'replies:>10': "posts with more than ten replies",
    }
    __about_values__ = f"a comparator ({ComparisonToken.__about_values__}) followed by a number"

    @classmethod
    def _value_parser(cls):
        return ComparisonToken._parser(pp.pyparsing_common.integer)

    def build_query(self):
        return self.value.compare(Post.reply_count)


class TagCountTag(NamedTag):
    """the `tags` tag specifies posts with some given amount of tags."""
    __about_name__ = "`tags`"
    key = 'tags'
    __about_examples__ = {
        'tags:=0': "posts with exactly zero tags",
        'tags:>10': "posts with more than ten tags",
    }
    __about_values__ = f"a comparator ({ComparisonToken.__about_values__}) followed by a number"

    @classmethod
    def _value_parser(cls):
        return ComparisonToken._parser(pp.pyparsing_common.integer)

    def build_query(self):
        return self.value.compare(Post.tag_count)
