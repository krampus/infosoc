"""Query builder utilities"""
from abc import ABC, abstractmethod

from ..db.models import Post


class MetaFilter(ABC):
    @abstractmethod
    def filter_query(self, querybuilder):
        """Apply mutations to a `QueryBuilder` object before any filters are applied"""
        pass


class QueryBuilder:
    def __init__(self):
        self.tree = None
        self.metafilters = []
        self.prefilters = []
        self.postfilters = []

        # Defaults:
        self.filter_removed = True
        self.order_by = Post.inserted_at.desc()

    def add_token(self, token):
        if isinstance(token, MetaFilter):
            self.metafilters.append(token)
        else:
            self.tree = token

    def add_tokens(self, tokens):
        for token in tokens:
            self.add_token(token)

    def build(self):
        for meta in self.metafilters:
            meta.filter_query(self)

        q = Post.query
        if self.filter_removed:
            q.filter_by(removed=False)
        for filter in self.prefilters:
            q = q.filter(filter)
        if self.tree is not None:
            q = q.filter(self.tree.build_query())
        for filter in self.postfilters:
            q = q.filter(filter)

        return q.order_by(self.order_by)

    def __str__(self):
        ret = "all posts"
        if self.tree:
            ret = str(self.tree)

        if self.metafilters:
            ret = f"{ret} ({', '.join([str(mt) for mt in self.metafilters])})"

        return ret
