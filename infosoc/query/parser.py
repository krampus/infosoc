"""An algebraic tag query parser built with pyparsing.

A tag query can be interpreted as a query for "the set of posts with
this tag".  We'll be abusing notation here: we use `and` to denote
intersection (the set of posts with tag A _and_ tag B), `or` to denote
union (posts with A _or_ B), `xor` to denote symmetric difference (A
or B but not both), and `not` for set complements (posts without tag
A).

The query syntax supported by this parser is much more powerful than
what's found in a typical imageboard. It can parse queries including
all of the aforementioned set operations, with grouping associativity
(i.e. parentheses).

* All operators are right-associative.
* Order of operations is `not`, `or`, `xor`, `and`.
* Operator symbols are currently `-`, `,`, and `^` for `not`, `or`,
  and `xor` respectively.
* Concatenation (whitespace) is used to denote `and`.

This largely allows for backwards compatibility with standard
imageboard query conventions. There's one notable exception to this
compatibility: most imageboards allow parentheses in tags
(i.e. `a_(2019)` is a valid tag), but this could lead to ambiguity in
parsing associativity, e.g. `-(tag_) tag_()` could be parsed as either
`not tag_) and not tag_(` or `not tag_ and tag_`. For this reason,
parentheses shouldn't be permitted in tags.

The trade-off is that parsing of certain valid queries can become
expensive very quickly, particularily queries with deeply-nested
expressions can take a very long time to parse...
TODO: implement simple parser for lightweight instances

Query Examples
==============

Tags can be negated:
* `-my_tag` -> `not my_tag` (i.e. all posts not tagged with `my_tag`)

Tags can be queried with set operations:
* `a b` -> `a and b` (i.e. all posts tagged with `a` and `b`)
* `a,b` -> `a or b` (i.e. all posts tagged with `a` or `b` or both)
* `a^b` -> `a xor b` (i.e. all posts tagged with `a` or `b`, but not both)

Other operators ignore whitespace:
* `a, b` -> `a or b`
* `a , b` -> `a or b`
* `a ^ b` -> `a xor b`

Operators are all right-associative, with order-of-operations:
* `a b c` -> `a and (b and c)`
* `a,b c` -> `(a or b) and c`
* `a,(b c)` -> `a or (b and c)`
* `-a, b ^ c -d` -> `(((not a) or b) xor c) and (not d)`
* `-(a, (b ^ (c -d)))` -> `not (a or (b xor (c and (not d))))`
"""
import pyparsing as pp

from ..app import db
from ..db import models
from ..config import config
from .builder import QueryBuilder, MetaFilter

# Memoize parsing. This speeds up parsing of nested expressions by quite a bit.
# From the documentation:
# > This speedup may break existing programs that use parse actions that have side-effects.
# So don't do that!
pp.ParserElement.enablePackrat()


def _operator(symbol):
    return pp.Suppress(pp.Literal(symbol)) if len(symbol) > 0 else None


class Tag:
    """tags denote some element of the content of a post.

    they're basically keywords you can use to describe posts, allowing
    you to easily search and explore posts based on their content. for
    example, a post with an image of a dog wearing sunglasses might be
    tagged with `dog` and `sunglasses`. searching for the tag `dog`
    would show you that post, along with every other post tagged with
    `dog`.
    """
    __about_name__ = 'tags'
    __about_examples__ = {
        'cool_post': "posts tagged with `cool_post`",
        'cool post': "posts tagged with both `cool` and `post`"
    }
    __about_values__ = f"any string without spaces matching the regular expression `/{config.validation.tag_pattern}/`"

    def __init__(self, tok):
        self.name = tok[0]
        self.aliases = config.instance.get_aliases

    def __str__(self):
        if config.instance.is_canon(self.name):
            return f'`{self.name}`'
        else:
            return f'`{self.name}`→`{config.instance.canon_name(self.name)}`'

    def build_query(self):
        return models.Post.tags.any(
            models.Tag.name.in_(config.instance.get_aliases(self.name))
        )

    def _tree(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.name == other.name

    @classmethod
    def _parser(cls):
        return (pp.Regex(config.validation.tag_pattern) + pp.NotAny(pp.Literal(':')))\
            .setParseAction(cls)\
            .setName('tag')


class KVTag:
    """Base class for `key:value` tags"""

    def __init__(self, tok):
        self.value = tok[0]

    def __str__(self):
        return f'{self.key}:{self.value}'

    def _tree(self):
        return {self.key: self.value._tree()}

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.key == other.key and self.value == other.value

    @classmethod
    def _parser(cls):
        return (
            pp.Suppress(
                pp.Literal(cls.key) + pp.Literal(':')
            ) + cls._value_parser()
        ).setParseAction(cls).setFailAction(cls._on_parse_failure)

    @classmethod
    def _sub_parsers(cls):
        return pp.Or([sub._parser() for sub in cls.__subclasses__() if hasattr(sub, 'key')])

    @classmethod
    def _on_parse_failure(cls, tok, loc, expr, err):
        # XXX very few parsing failure cases actually fail on the problem token so this isn't really useful
        raise pp.ParseFatalException(f"Unknown value `{expr}` for key {cls.key}")


class NamedTag(KVTag):
    """named tags are special tags used to filter query results by certain properties.

    named tags are made up of a key (or "name") and a value, separated
    by a colon (`:`). unlike meta-tags, named tags can be used in
    queries just like regular tags.
    """
    __about_name__ = "named tags"

    @classmethod
    def _value_parser(cls):
        return Tag._parser()

    @classmethod
    def __about_subsections__(cls):
        from . import special_tags  # noqa: F401
        return cls.__subclasses__()


class MetaTag(KVTag, MetaFilter):
    """meta-tags are special tags used to modify the query as a whole.

    like named tags, meta-tags are made up of a key and a value
    separated by a colon (`:`). since meta-tags modify the entire
    query, they can't be used in algebraic query expressions, and must
    be placed at the start or end of the query. a query like
    `cool_post, sort:likes` would fail with an error, because it
    doesn't really make any sense.
    """
    __about_name__ = "meta-tags"

    def _tree(self):
        return {self.key: self.value}

    @classmethod
    def _value_parser(cls):
        return pp.oneOf(cls.values).setFailAction(cls._on_parse_failure)

    @classmethod
    def __about_subsections__(cls):
        from . import special_tags  # noqa: F401
        return cls.__subclasses__()


class ComparisonToken:
    """Parser token for value comparison filters"""
    _comparators = {
        '=': lambda a, b: a == b,
        '!=': lambda a, b: a != b,
        '<': lambda a, b: a < b,
        '<=': lambda a, b: a <= b,
        '>': lambda a, b: a > b,
        '>=': lambda a, b: a >= b,
    }
    values = list(_comparators.keys())
    __about_values__ = f"one of {','.join(['`' + v + '`' for v in values[:-1]]) + ', or `' + values[-1] + '`'}"

    def __init__(self, tok):
        self._comparator = tok[0]
        self._compare = self._comparators[self._comparator]
        self.value = tok[1]

    def compare(self, other):
        return self._compare(other, self.value)

    def __str__(self):
        return f"{self._comparator}{self.value}"

    def _tree(self):
        return (self.key, self.value)

    @classmethod
    def _parser(cls, value_token):
        return (
            pp.oneOf(cls._comparators.keys())
            + value_token
        ).setParseAction(cls)


class BinaryOp:
    """An operation with two operands, using infix notation by default"""

    def __init__(self, tok):
        self.a, self.b = tok[0][:2]

    def __str__(self):
        return f'({self.a} {self.str_symbol} {self.b})'

    def _tree(self):
        return {self.str_symbol: [self.a._tree(), self.b._tree()]}

    @classmethod
    def infix(cls):
        return (_operator(cls.symbol), 2, pp.opAssoc.RIGHT, cls)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.a == other.a and self.b == other.b


class UnaryOp:
    """An operation with one operand using prefix notation by default"""

    def __init__(self, tok):
        self.a = tok[0][0]

    def __str__(self):
        return f'({self.str_symbol} {self.a})'

    def _tree(self):
        return {self.str_symbol: [self.a._tree()]}

    @classmethod
    def infix(cls):
        return (_operator(cls.symbol), 1, pp.opAssoc.RIGHT, cls)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.a == other.a


class AndOp(BinaryOp):
    """the "and" operation produces the *set intersection* of two subqueries.

    in other words, you can search for posts that match one tag *and*
    another tag. since this kind of query is so common, you denote
    this by simply concatenating the terms together with a space
    between them: the query `a b` finds the set of posts tagged with
    both `a` and `b`.
    """
    __about_name__ = '"and" operator'
    __about_examples__ = {
        'cool dog': "posts tagged with both `cool` and `dog`",
        'cool dog sunglasses': "posts tagged with `cool` and `dog` and `sunglasses`"
    }
    symbol = ''
    str_symbol = 'and'

    def build_query(self):
        return db.and_(self.a.build_query(), self.b.build_query())


class OrOp(BinaryOp):
    """the "or" operation produces the *set union* of two subqueries.

    in other words, you can search for posts that match one tag *or*
    another tag, **or both**. you denote this with a comma (`,`)
    between the terms: the query `a, b` finds the set of posts tagged
    with **`a` or `b` (or both)**. whitespace around the `,`
    operator is ignored, so the following queries are equivalent:

* `a,b`
* `a, b`
* `a , b`
* `a ,b`
    """
    __about_name__ = '"or" operator'
    __about_examples__ = {
        'cool, dog': "posts tagged with `cool` or with `dog` (or with both)",
        'cool, dog, sunglasses': "posts tagged with `cool`, or `dog`, or `sunglasses` (or any combination of the three)"
    }
    symbol = ','
    str_symbol = 'or'

    def build_query(self):
        return db.or_(self.a.build_query(), self.b.build_query())


class XorOp(BinaryOp):
    """the "xor" operation produces the *set symmetric difference* ("xor") of two subqueries.

    in other words, you can search for posts that match one tag *or*
    another tag, **but not both**. you denote this with a caret (`^`)
    between the terms: the query `a ^ b` finds the set of posts tagged
    with **either `a` or `b` (but not both)**. whitespace around the
    `^` operator is ignored, so the following queries are equivalent:

* `a^b`
* `a^ b`
* `a ^ b`
* `a ^b`
    """
    __about_name__ = '"xor" operator'
    __about_examples__ = {
        'cool ^ dog': "posts tagged with `cool`, or with `dog`, **but not with both `cool` and `dog`**"
    }
    symbol = '^'
    str_symbol = 'xor'

    def build_query(self):
        qa, qb = self.a.build_query(), self.b.build_query()
        return db.and_(db.or_(qa, qb), db.not_(db.and_(qa, qb)))


class NotOp(UnaryOp):
    """the "not" operation produces the *set complement* of two subqueries.

    in other words, you can search for posts that *do not* match a
    tag. you denote this with a dash (`-`) before the term: the query
    `-a` finds the set of posts **not** tagged with `a`. since the
    character "-" can appear inside a tag, it's important to put
    whitespace before the `-` operator. for example, while `a -b`
    would find the set of posts tagged with `a` and **not** `b`, `a-b`
    would find the set of posts tagged with `a-b`.
    """
    __about_name__ = '"not" operator'
    __about_examples__ = {
        '-cool': "posts **not** tagged with `cool`",
        '-cool cat': "posts tagged with `cat`, but **not** with `cool`",
        '-(cool cat)': "posts **not** tagged with both `cool` and `cat`"
    }
    symbol = '-'
    str_symbol = 'not'

    def build_query(self):
        return db.not_(self.a.build_query())


class Parser:
    def __init__(self):

        from . import special_tags  # noqa: F401
        named_tag = NamedTag._sub_parsers()
        meta_tag = MetaTag._sub_parsers()

        special_keys = [pp.Literal(t.key) + pp.Literal(':')
                        for sub in KVTag.__subclasses__() for t in sub.__subclasses__()]

        tag = (
            pp.NotAny(pp.Or(special_keys))
            + Tag._parser()
        )

        expression = pp.infixNotation(
            named_tag | tag,
            [
                NotOp.infix(),
                OrOp.infix(),
                XorOp.infix(),
                AndOp.infix()
            ],
        )
        self.parser = pp.Optional(
            pp.ZeroOrMore(meta_tag)
            + pp.Optional(expression)
            + pp.ZeroOrMore(meta_tag)
        )

    def parse_query(self, query_string):
        qb = QueryBuilder()
        qb.add_tokens(self.parser.parseString(query_string, parseAll=True))
        return qb
