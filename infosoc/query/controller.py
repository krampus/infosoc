"""Controller methods for search queries"""
from pyparsing import ParseException, ParseFatalException
from flask import flash

from .parser import Parser

parser = Parser()


def posts(query=''):
    """Return the posts matched by this tag query, most recent first"""
    try:
        q = parser.parse_query(query)
    except ParseFatalException as ex:
        flash(f"error in tag query: {ex.msg}", category='alert-warning')
        q = parser.parse_query('')
    except ParseException as ex:
        err_end = ex.pstr.find(' ', ex.loc)
        err_substr = ex.pstr[ex.loc:err_end if err_end > 0 else None]
        flash(f"error in tag query: {err_substr}", category='alert-warning')
        q = parser.parse_query('')

    return q
