"""Flask CLI commands"""
import click
from flask import current_app
from flask.cli import with_appcontext


def init_app(app):

    def cli_command(fn):
        """Decorator version of app.cli.add_command"""
        app.cli.add_command(fn)
        return fn

    @cli_command
    @click.command('init-db')
    @with_appcontext
    def init_db_cmd():
        """Reinitialize the database, without clearing existing data"""
        from .app import db
        from .db import models  # noqa: F401
        db.create_all()
        click.echo("Database initialized.")

    @cli_command
    @click.command('drop-table')
    @click.argument('tablename')
    @with_appcontext
    def drop_table_cmd(tablename):
        """Drop a table from the database"""
        from .app import db
        from .db import models  # noqa: F401
        if tablename in db.Model.metadata.tables:
            db.Model.metadata.tables[tablename].drop(db.engine)
            click.echo(f"Dropped table `{tablename}`")
        else:
            raise click.ClickException(f"No such table `{tablename}`")

    @cli_command
    @click.command('notify-all')
    @click.option('--category', '-c', default=['alert-info'], multiple=True,
                  help="category used to render this notification")
    @click.option('--force', default=False, is_flag=True, help="do not prompt for confirmation")
    @click.argument('message')
    @with_appcontext
    def notify_all_cmd(category, message, force):
        """Broadcast a notification to all users of this instance"""
        from .app import db
        from .db.models import Notification, User

        categories = ' '.join(category)
        click.echo(f"Your message:\n\n{message}\n")
        click.echo(f"with categories '{categories}'")
        new_notifications = [
            Notification(
                category=categories,
                content=message,
                user=user
            ) for user in User.query
        ]

        if not force:
            click.confirm(f"Send to {len(new_notifications)} users?", abort=True)

        db.session.add_all(new_notifications)
        db.session.commit()
        click.echo("Done.")

    @cli_command
    @click.command('add-user')
    @click.argument('nickname')
    @click.argument('password')
    @click.option('--name', '-n', default=None, help="user display name")
    @click.option('--email', '-m', default=None, help="user email")
    def add_user_cmd(nickname, password, name, email):
        """Add a user to the instance"""
        from .auth import add_user
        click.echo(f"Adding new user @{nickname}...")
        add_user(nickname, password, name, email)
        click.echo("Done.")

    @cli_command
    @click.command('rm-user')
    @click.option('--nickname', '-n', multiple=True, help="nickname of user to delete")
    @click.option('--id', '-i', multiple=True, help="id of user to delete")
    @click.option('--force', default=False, is_flag=True, help="do not prompt for confirmation")
    def rm_user_cmd(nickname, id, force):
        """Delete one or more users from the instance, by nickname or by ID"""
        assert len(nickname) > 0 or len(id) > 0, "Must specify at least one user by nickname or id"

        from .app import db
        from .db.models import User

        users = User.query.filter(db.or_(User.id.in_(id), User.nickname.in_(nickname))).all()
        click.echo(f"Will delete the following users:\n{users}\n")
        if not force:
            click.confirm(f"Proceed?", abort=True)

        for u in users:
            db.session.delete(u)

        db.session.commit()
        click.echo("Done.")

    @cli_command
    @click.command('moderator')
    @click.argument('nickname')
    @click.option('--demod', '-d', default=False, is_flag=True,
                  help="demote the user, removing moderator ability")
    @click.option('--no-notify', '-q', default=False, is_flag=True,
                  help="do not notify the user of their change in status")
    @click.option('--force', default=False, is_flag=True,
                  help="do not prompt for confirmation")
    def moderator_cmd(nickname, demod, no_notify, force):
        """Give or remove moderator abilities to a user of this instance"""
        from .app import db
        from .db.models import User
        from . import notification
        from sqlalchemy.orm.attributes import flag_modified

        user = User.query.filter_by(nickname=nickname).first()

        assert user is not None, f"No user with nickname `{nickname}` found."

        if demod:
            assert user.is_moderator, f"{user.handle} is not a moderator."

            if not force:
                click.confirm(f"REMOVE moderator abilities from {user.handle}?", abort=True)

            user.info['moderator'] = False
        else:
            assert not user.is_moderator, f"{user.handle} is already a moderator."

            if not force:
                click.confirm(f"GIVE moderator abilities to {user.handle}?", abort=True)

            user.info['moderator'] = True

        flag_modified(user, 'info')
        db.session.commit()

        if not no_notify:
            if demod:
                notification.on_demod(user)
            else:
                notification.on_mod(user)

        click.echo("Done.")

    @cli_command
    @click.command('clean-media')
    @click.option('--dry-run', is_flag=True, help="Do not delete unused media files, just show them")
    @with_appcontext
    def clean_media(dry_run):
        """Clean the media store, removing all files not associated with application objects"""

        # Find database rows not associated with anything in the application
        from .app import db
        from .db.models import User, Post, Media
        unused_row_q = (
            Media.query
            .filter(Media.post is None)  # not post content
            .outerjoin(Post, Post.thumbnail_id == Media.id).filter(Post.id is None)  # not a thumbnail
            .outerjoin(User, User.avatar_id == Media.id).filter(User.id is None)  # not an avatar
        )

        unused_rows = unused_row_q.all()
        if len(unused_rows) > 0:
            for row in unused_rows:
                click.echo(f"Unused media reference in database: {row}")
            if not dry_run:
                unused_row_q.delete()
                db.session.commit()
                click.echo("Deleted unused media references.")
        else:
            click.echo(f"No unused media in database.")

        # Find unused files in media storage
        unused_files = []
        for f in current_app.config['MEDIA_PATH'].iterdir():
            try:
                # note, if Media.id changes this will break
                f_id = int(f.stem)
                if Media.query.get(f_id) is None:
                    unused_files.append(f)
            except ValueError:
                # bad id
                unused_files.append(f)

        if len(unused_files) > 0:
            for f in unused_files:
                click.echo(f"Unused file in media store: {f}")
                if not dry_run:
                    f.unlink()
                    click.echo(f"Deleted {f}")
        else:
            click.echo(f"No unused files in media store.")
