"""Authentication & registration view"""
import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from werkzeug.security import check_password_hash, generate_password_hash

from .validation import validate_email, validate_nickname, validate_password, validate_text
from .app import db, alert_on_exception
from .user import this_user_required
from .db.models import User
from .config import config

bp = Blueprint('auth', __name__, url_prefix='/auth')


def add_user(nickname, password, name=None, email=None):
    nickname = validate_nickname(nickname)
    password = validate_password(password)
    email = validate_email(email)
    name = validate_text(name) or nickname

    # registration info is valid
    new_user = User(
        email=email,
        nickname=nickname,
        name=name,
        local=True,
        password_hash=generate_password_hash(password)
    )
    db.session.add(new_user)
    db.session.commit()
    return new_user


@bp.route('/register', methods=('GET', 'POST'))
@alert_on_exception()
def register():
    if not config.instance.registrations_open:
        flash('registrations are currently closed', category='alert-danger')
        return redirect(url_for('index'))
    else:
        if request.method == 'POST':
            assert request.form['tosCheck'], 'must agree to the terms of service'
            assert request.form['password'] == request.form['passwordCheck'], 'password confirmation does not match'

            new_user = add_user(
                nickname=request.form['nickname'],
                password=request.form['password'],
                email=request.form['email'],
                name=request.form['name']
            )

            session.clear()
            session['user_id'] = new_user.id

            return redirect(url_for('auth.login'))
        else:
            return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
@alert_on_exception()
def login():
    if request.method == 'POST':
        nickname = request.form['nickname']
        password = request.form['password']
        user = User.query.filter_by(nickname=nickname).first()
        assert user is not None, f'unknown user `{nickname}`'
        assert check_password_hash(user.password_hash, password), 'incorrect password'
        session.clear()
        session['user_id'] = user.id
        return redirect(url_for('index'))

    return render_template('auth/login.html')


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


@bp.route('/edit/<id>', methods=['POST'])
@this_user_required
@alert_on_exception()
def edit_account(user):
    assert check_password_hash(user.password_hash, request.form['password']), 'incorrect password'
    user.email = validate_email(request.form['email'])

    new_password = request.form['newPassword']
    if len(new_password) > 0:
        assert validate_password(new_password) == request.form['newPasswordCheck'],\
            'password confirmation does not match'
        user.password_hash = generate_password_hash(new_password)

    db.session.commit()
    flash(f'saved account data for user {user.handle}', category='alert-success')
    return redirect(url_for('user.posts', nickname=user.nickname))


@bp.before_app_request
def load_current_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = User.query.get(user_id)


def login_required(fn):
    @functools.wraps(fn)
    def inner(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        else:
            return fn(**kwargs)
    return inner
