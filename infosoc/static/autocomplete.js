/*
 * I'd like to minimize JS usage for privacy concerns, and also
 * because I don't really know JS. As a firm rule, all application
 * functionality should be accessible _without_ javascript. Great
 * deliberation should be taken before including any javascript
 * dependency.
 */

// This should NOT be changed when the site config tag pattern changes
SUB_PATTERN = /[^\s,^()]*$/;

// Cache suggestion results by URL
AC_CACHE = {};

/* Get the autocomplete wrapper containing an element
 */
function _get_ac_wrapper(el) {
    if (el.parentElement === null || el.classList.contains('ac-wrapper')) {
        return el;
    } else {
        return _get_ac_wrapper(el.parentElement);
    }
}

/* Initialize tag autocompletion on an input element by ID
 */
function autocomplete(input_id, url) {
    // get input element, wrapper element, and list if it exists, otherwise create it.
    var input = document.getElementById(input_id);
    var wrapper = _get_ac_wrapper(input);
    var lists = wrapper.getElementsByClassName('ac-list');
    var ac;
    if (lists.length > 0) {
        ac = lists[0];
    } else {
        ac = document.createElement('ul');
        ac.classList.add('ac-list');
        wrapper.appendChild(ac);
    }

    var selection = 0;

    // Depopulate and hide autocomplete popup
    function teardown() {
        ac.innerHTML = ''
    }

    // Populate autocomplete elements from response
    function populate(suggestions, cursor) {
        ac.innerHTML = "";

        suggestions.forEach(function(tag) {
            // build suggestion list element
            function do_fill(event) {
                event.preventDefault()
                var after = input.value.slice(cursor);
                var before = input.value.slice(0, cursor).replace(SUB_PATTERN, tag);
                input.value = before + after;
                teardown();
                input.focus();
                input.setSelectionRange(before.length, before.length);
            }

            var a = document.createElement('a');
            a.classList.add('list-group-item');
            a.classList.add('icon-tag');
            a.innerText = tag;
            a._action = do_fill;
            a.href = "#";
            a.addEventListener('click', do_fill);
            ac.appendChild(a);

        });

        if(suggestions.length > 0) {
            // select first suggestion
            select(0);
        }
    }

    // Set the currently-selected index of the autocomplete list
    function select(idx) {
        // clear selection
        Array.from(ac.children).forEach(el => el.classList.remove('bg-light'));

        // select new element
        selection = idx;
        ac.children[idx].classList.add('bg-light');
    }

    // Run autocomplete routine when appropriate on input
    input.addEventListener('input', function(e) {
        if (['insertText', 'deleteContentBackward'].includes(e.inputType)) {
            var cursor = input.selectionStart;
            var stub = input.value.slice(0, cursor).match(SUB_PATTERN)[0] || '';

            if (stub in AC_CACHE) {
                populate(AC_CACHE[stub], cursor);
            } else {
                axios.get(url, {
                    params: {stub: stub || ''}
                })
                    .then(function(response) {
                        AC_CACHE[stub] = response.data;
                        populate(response.data, cursor);
                    });
            }
        }
    })

    // Handle keydown events as keyboard navigation
    input.addEventListener('keydown', function(e) {
        if (ac.childElementCount > 0) {
            // autocomplete is active
            if (e.keyCode == 9) {
                // tab
                e.preventDefault();
                ac.children[selection]._action(e);
            } else if (e.keyCode == 38) {
                // up arrow
                e.preventDefault();
                select((selection + ac.childElementCount - 1) % ac.childElementCount);
            } else if (e.keyCode == 40) {
                // down arrow
                e.preventDefault();
                select((selection + 1) % ac.childElementCount);
            } else if (e.keyCode == 27) {
                // escape
                teardown();
            }
        }
    });

    // Teardown autocomplete elements on loss of focus
    input.addEventListener('focusout', function(e) {
        // delay wiping content until click events have fired
        window.setTimeout(function() {
            teardown();
        }, 200);
    });
}
