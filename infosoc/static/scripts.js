// Python boolean interoperability
const True = true;
const False = false;


function _get_relation_by_class_name(el, className, relation) {
    var related = el[relation];
    if (related === null || related.classList.contains(className)) {
        return related;
    } else {
        return _get_relation_by_class_name(related, className, relation);
    }
}

function getAncestorByClassName(el, className) {
    return _get_relation_by_class_name(el, className, 'parentElement');
}

function getNextSiblingByClassName(el, className) {
    return _get_relation_by_class_name(el, className, 'nextElementSibling');
}

function playNext(element, mediaTag, loop) {
    var mediaClass = `${mediaTag}-media-wrapper`;
    var wrapper = getAncestorByClassName(element, mediaClass);
    if (wrapper) {
        var next_wrapper = getNextSiblingByClassName(wrapper, mediaClass);
        if (next_wrapper === null) {
            if (loop) {
                next_wrapper = document.getElementsByClassName(mediaClass)[0];
            } else {
                return;
            }
        }
        var next = next_wrapper.getElementsByTagName(mediaTag)[0];
        next.play();
    }
}

function playNextAudio(element, loop) {
    return playNext(element, 'audio', loop);
}

function playNextVideo(element, loop) {
    return playNext(element, 'video', loop);
}

function post_dismiss(event, element, url) {
    // If JS is enabled, dynamically dismiss notification, ignoring redirects
    element.remove();
    event.preventDefault();
    axios.post(url);
}

function post_like(event, element, url) {
    // If JS is enabled, dynamically post like
    event.preventDefault();
    element.classList.toggle('icon-star');
    element.classList.toggle('icon-star-empty');
    axios.post(url);
}
