"""SQLAlchemy ORM models"""
from datetime import datetime
from sqlalchemy.ext.hybrid import hybrid_property

from ..app import db
from .mimetypes import MIMEType, ImageTypes, VideoTypes, AudioTypes, BinaryTypes, DocumentTypes, MultimediaTypes
from ..config import config


def default_for(*args, **kwargs):
    """Wrap the underlying function in a column, with the function being the default value factory"""
    def decorator(fn):
        return db.Column(*args, default=fn, **kwargs)
    return decorator


class Timestamped:
    inserted_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now, onupdate=datetime.now)


_post_tags = db.Table(
    'post_tags',
    db.Model.metadata,
    db.Column('post_id', db.Integer, db.ForeignKey('posts.id')),
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'))
)

_user_likes = db.Table(
    'user_likes',
    db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('post_id', db.Integer, db.ForeignKey('posts.id')),
)


class User(db.Model, Timestamped):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    deactivated = db.Column(db.Boolean, default=False, nullable=False)

    # user data
    email = db.Column(db.UnicodeText)
    password_hash = db.Column(db.UnicodeText, nullable=False)
    nickname = db.Column(db.UnicodeText, nullable=False)
    name = db.Column(db.UnicodeText, default=lambda ctx: ctx.current_parameters.get('nickname'))
    bio = db.Column(db.UnicodeText)
    avatar_id = db.Column(db.Integer, db.ForeignKey('media.id'))

    avatar = db.relationship('Media', single_parent=True,
                             backref=db.backref('avatar_for'),
                             cascade="all, delete-orphan")
    posts = db.relationship('Post', back_populates='author',
                            single_parent=True, cascade="all, delete-orphan",
                            order_by="desc(Post.updated_at)")
    liked_posts = db.relationship('Post', secondary=_user_likes,
                                  back_populates='liked_by', order_by="desc(Post.updated_at)")
    notifications = db.relationship('Notification', back_populates='user',
                                    single_parent=True, cascade="all, delete-orphan",
                                    order_by="desc(Notification.updated_at)")

    # activitypub data
    local = db.Column(db.Boolean, nullable=False, default=True)
    following = db.Column(db.JSON, nullable=False, default={})

    @default_for(db.UnicodeText, nullable=False)
    def ap_id(ctx):
        return f'{config.instance.url}/users/{ctx.current_parameters.get("nickname")}'

    @default_for(db.UnicodeText, nullable=False)
    def follower_address(ctx):
        return f'{ctx.current_parameters.get("ap_id")}/followers'

    @default_for(db.JSON, nullable=False)
    def info(ctx):
        return dict(
            blocks=[],
            mod_notes='',
            moderator=False,
            **(config.preferences.__dict__)
        )

    @property
    def handle(self):
        return f'@{self.nickname}'

    @property
    def icon(self):
        """Determine the fontello glyph to associate with this user"""
        if self.local:
            if self.is_moderator:
                return 'icon-shield'
            else:
                return 'icon-user'
        else:
            return 'icon-globe'

    @property
    def is_moderator(self):
        return self.info.get('moderator', False)

    def __str__(self):
        return self.handle


class Media(db.Model, Timestamped):
    __tablename__ = 'media'

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.UnicodeText, nullable=False)
    mime_type = db.Column(db.Enum(MIMEType), nullable=False)
    path = db.Column(db.UnicodeText, nullable=False)
    md5sum = db.Column(db.UnicodeText, nullable=False)
    info = db.Column(db.JSON, default={}, nullable=False)

    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))
    post = db.relationship('Post', back_populates='media', foreign_keys=[post_id])

    def type_in(self, enum):
        return self.mime_type.name in enum.__members__

    @property
    def is_image(self):
        return self.type_in(ImageTypes)

    @property
    def is_video(self):
        return self.type_in(VideoTypes)

    @property
    def is_audio(self):
        return self.type_in(AudioTypes)

    @property
    def is_document(self):
        return self.type_in(DocumentTypes)

    @property
    def is_binary(self):
        return self.type_in(BinaryTypes)

    @property
    def is_multimedia(self):
        return self.type_in(MultimediaTypes)

    @property
    def icon(self):
        if self.is_image:
            return 'icon-picture'
        elif self.is_video:
            return 'icon-video'
        elif self.is_audio:
            return 'icon-music'
        elif self.is_multimedia:
            return 'icon-flash'
        elif self.is_document:
            return 'icon-doc-text-inv'
        else:
            return 'icon-doc-inv'

    @property
    def store_filename(self):
        return f'{self.id}.{self.mime_type.name}'

    def __str__(self):
        return self.store_filename

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.id}: {self} ({self.filename})>"


class Post(db.Model, Timestamped):
    __tablename__ = 'posts'

    id = db.Column(db.Integer, primary_key=True)
    removed = db.Column(db.Boolean, default=False, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    thumbnail_id = db.Column(db.Integer, db.ForeignKey('media.id'))
    reply_to_id = db.Column(db.Integer, db.ForeignKey('posts.id'))
    subject = db.Column(db.UnicodeText)
    description = db.Column(db.UnicodeText)

    author = db.relationship('User', back_populates='posts')
    thumbnail = db.relationship('Media', foreign_keys=[thumbnail_id],
                                backref=db.backref('thumbnail_for'),
                                single_parent=True,
                                cascade='all, delete-orphan')
    replies = db.relationship('Post', foreign_keys=[reply_to_id],
                              backref=db.backref('reply_to', remote_side=[id]))
    media = db.relationship('Media', back_populates='post',
                            foreign_keys=[Media.post_id],
                            single_parent=True,
                            cascade='all, delete-orphan',
                            order_by="asc(Media.id)")
    tags = db.relationship('Tag', secondary=_post_tags,
                           back_populates='posts', order_by="asc(Tag.name)")
    liked_by = db.relationship('User', secondary=_user_likes,
                               back_populates='liked_posts', order_by="asc(User.nickname)")

    @hybrid_property
    def like_count(self):
        return len(self.liked_by)

    @like_count.expression
    def like_count(cls):
        return (
            db.select([db.func.count(_user_likes.c.user_id)])
            .where(_user_likes.c.post_id == cls.id)
            .label('like_count')
        )

    @hybrid_property
    def tag_count(self):
        return len(self.tags)

    @tag_count.expression
    def tag_count(cls):
        return (
            db.select([db.func.count(_post_tags.c.tag_id)])
            .where(_post_tags.c.post_id == cls.id)
            .label('tag_count')
        )

    @hybrid_property
    def reply_count(self):
        return len(self.replies)

    @reply_count.expression
    def reply_count(cls):
        reply = db.aliased(Post)
        return (
            db.select([db.func.count(reply.id)])
            .where(reply.reply_to_id == cls.id)
            .label('reply_count')
        )

    def _media_of_type(self, enum):
        return [f for f in self.media if f.type_in(enum)]

    @property
    def images(self):
        return self._media_of_type(ImageTypes)

    @property
    def videos(self):
        return self._media_of_type(VideoTypes)

    @property
    def audio(self):
        return self._media_of_type(AudioTypes)

    @property
    def documents(self):
        return self._media_of_type(DocumentTypes)

    @property
    def binaries(self):
        return self._media_of_type(BinaryTypes)

    @property
    def multimedia(self):
        return self._media_of_type(MultimediaTypes)

    @property
    def filenames(self):
        return ', '.join([f.filename for f in self.media])

    def __repr__(self):
        desc = [
            f'{len(self.media)} media',
            f'{len(self.tags)} tags',
        ]
        if self.author:
            desc.append(f'author={self.author.handle}')
        if self.thumbnail:
            desc.append(f'thumbnail={self.thumbnail}')
        if self.subject:
            desc.append(f'subject="{self.subject[:16] + (self.subject[16:] and "...")}"')
        if self.description:
            desc.append(f'description="{self.description[:24] + (self.description[24:] and "...")}"')

        return f"<{self.__class__.__name__} {self.id}: {'; '.join(desc)}>"


# TODO this should probably be replaced by some kind of virtual tag relation
class Tag(db.Model, Timestamped):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.UnicodeText, nullable=False, unique=True)

    posts = db.relationship('Post', secondary=_post_tags, back_populates='tags', order_by="desc(Post.updated_at)")

    @hybrid_property
    def post_count(self):
        return len(self.posts)

    @post_count.expression
    def post_count(cls):
        return (
            db.select([db.func.count(_post_tags.c.post_id)])
            .where(_post_tags.c.tag_id == cls.id)
            .label('post_count')
        )

    def __str__(self):
        return f"#{self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.id}: {self}>"


class Notification(db.Model, Timestamped):
    __tablename__ = 'notifications'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    category = db.Column(db.UnicodeText, default="alert-info")
    content = db.Column(db.UnicodeText)
    href = db.Column(db.UnicodeText)

    user = db.relationship('User', back_populates='notifications')


__all__ = ['User', 'Media', 'Post', 'Tag', 'Notification']
