"""Resources for handling content by MIME type

For reference:
(list of important web types)
https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
(list of all recognized MIME types)
http://www.iana.org/assignments/media-types/media-types.xhtml
"""
from enum import Enum
from itertools import chain


class ImageTypes(Enum):
    BMP = 'image/bmp'
    GIF = 'image/gif'
    ICO = 'image/vnd.microsoft.icon'
    JPEG = 'image/jpeg'
    PNG = 'image/png'
    SVG = 'image/svg+xml'
    TIFF = 'image/tiff'
    WEBP = 'image/webp'


class VideoTypes(Enum):
    AVI = 'video/x-msvideo'
    MPEG = 'video/mpeg'
    MP4 = 'video/mp4'
    OGV = 'video/ogg'
    TS = 'video/mp2t'
    WEBM = 'video/webm'


class AudioTypes(Enum):
    MIDI = 'audio/midi'
    MID = 'audio/x-midi'
    MP3 = 'audio/mp3'
    OGA = 'audio/ogg'
    WAV = 'audio/wav'
    WEBA = 'audio/webm'


class BinaryTypes(Enum):
    BZ = 'application/x-bzip'
    BZ2 = 'application/x-bzip2'
    JAR = 'application/java-archive'
    RAR = 'application/x-rar-compressed'
    TAR = 'application/x-tar'
    ZIP = 'application/zip'
    BIN = 'application/octet-stream'


class DocumentTypes(Enum):
    AZW = 'application/vnd.amazon.ebook'
    DOC = 'application/msword'
    DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    EPUB = 'application/epub+zip'
    HTML = 'text/html'
    TXT = 'text/plain'
    MD = 'text/markdown'
    JS = 'text/javascript'
    JSON = 'application/json'
    ODP = 'application/vnd.oasis.opendocument.presentation'
    ODS = 'application/vnd.oasis.opendocument.spreadsheet'
    ODT = 'application/vnd.oasis.opendocument.text'


class MultimediaTypes(Enum):
    SWF = 'application/x-shockwave-flash'


MIMEType = Enum(
    'MIMEType',
    [(x.name, x.value) for x in chain(ImageTypes, VideoTypes, AudioTypes, BinaryTypes, DocumentTypes, MultimediaTypes)]
)
MIMEType.__doc__ = """An incomplete collection of MIME types

Note for developers: member names should correspond to the most
common file extension for the given MIME type. Some functionality
may rely on this, so take care when extending this collection.
"""
