"""Markup filters for application-specific formatting"""
import re
from jinja2 import Markup
from flask import url_for

from .config import config

# TODO does this actually work
_hashtag_pattern = re.compile(f'\B#(?P<tag>{config.validation.tag_pattern})')  # noqa: W605

# Stolen from Pleroma ;*
_mention_pattern = re.compile(
    r"\B@(?P<nickname>[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+)(?:@(?P<instance>[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*))?"  # noqa: E501
)


def sub_hashtag(match):
    href = url_for('index.results', query=match.group('tag'))
    return f'<a class="tag-link" href="{href}">{match.group()}</a>'


def sub_mention(match):
    instance = match.group('instance')
    if not instance or instance == config.instance.url.netloc:
        href = url_for('user.posts', nickname=match.group('nickname'))
        return f'<a class="mention-link" href="{href}">{match.group()}</a>'
    else:
        # TODO lookup federated users
        return match.group()


def render_all(text):
    return Markup(
        _mention_pattern.sub(
            sub_mention,
            _hashtag_pattern.sub(
                sub_hashtag,
                text
            )
        )
    )


def init_app(app):
    app.jinja_env.filters['appfmt'] = render_all
