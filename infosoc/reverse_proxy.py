class ReverseProxied(object):
    """Reverse proxy drop-in for Flask.

    Flask's ``url_for`` can get wonky behind a reverse-proxy. This
    patches the wsgi app to correctly build URLs with a prefix.

    Usage
    =====
        app = Flask(__name__)
        app.wsgi_app = ReverseProxied(app.wsgi_app, script_name='/app_location')

    See
    ===
    https://blog.macuyiko.com/post/2016/fixing-flask-url_for-when-behind-mod_proxy.html
"""
    def __init__(self, app, script_name=None, scheme=None, server=None):
        self.app = app
        self.script_name = script_name
        self.scheme = scheme
        self.server = server

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '') or self.script_name
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]
        scheme = environ.get('HTTP_X_SCHEME', '') or self.scheme
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        server = environ.get('HTTP_X_FORWARDED_SERVER', '') or self.server
        if server:
            environ['HTTP_HOST'] = server
        return self.app(environ, start_response)
