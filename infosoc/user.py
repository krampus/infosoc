"""User views"""
import functools
from flask import Blueprint, render_template, redirect, url_for, flash, g, request, current_app
from sqlalchemy.orm.attributes import flag_modified

from . import media, query
from .index import paginated
from .app import db, alert_on_exception, preference
from .db.models import User, Post
from .validation import validate_text, validate_tag, validate_image

_avatar_dim = 128, 128  # TODO should this be configurable?

bp = Blueprint('user', __name__, url_prefix='/user')


def valid_user_required(fn):
    @functools.wraps(fn)
    def inner(**kwargs):
        user = User.query.filter_by(**kwargs).first()
        if user is not None:
            return fn(user)
        else:
            flash(f"no such user with {kwargs} on this instance", category='alert-danger')
            return redirect(url_for('index'))
    return inner


def this_user_required(fn):
    @functools.wraps(fn)
    def inner(user):
        if g.user and g.user.id == user.id:
            return fn(user)
        else:
            flash(f"only {user.handle} can do that", category='alert-danger')
            return redirect(url_for('user.posts', nickname=user.nickname))
    return valid_user_required(inner)


def moderator_required(fn):
    @functools.wraps(fn)
    def inner(*args, **kwargs):
        if g.user and g.user.is_moderator:
            return fn(*args, **kwargs)
        else:
            flash(f"only moderators may do that", category='alert-danger')
            return redirect(url_for('index'))
    return inner


@bp.route('/<nickname>')
@bp.route('/<nickname>/posts')
@valid_user_required
@paginated
def posts(user, pager):
    query_string = f'author:{user.nickname}'

    # TODO cache
    qb = query.posts(query_string)
    page = pager(qb.build())

    return render_template(
        'user/posts.html',
        user=user,
        query=query_string,
        page=page
    )


@bp.route('/<nickname>/likes')
@valid_user_required
@paginated
def likes(user, pager):
    if (not preference('hide_likes', user=user)) or (g.user and g.user == user):
        page = pager(Post.query.filter(Post.liked_by.any(id=user.id)))
        return render_template(
            'user/likes.html',
            user=user,
            page=page
        )
    else:
        flash(f"{user.handle}'s favorites are private", category='alert-danger')
        return redirect(url_for('user.posts', nickname=user.nickname))


@bp.route('/<nickname>/following')
@valid_user_required
@paginated
def following(user, pager):
    # TODO
    q = User.query
    page = pager(q)
    return render_template(
        'user/related.html',
        title=f"users followed by {user.handle}",
        user=user,
        page=page
    )


@bp.route('/<nickname>/followers')
@valid_user_required
@paginated
def followers(user, pager):
    # TODO
    q = User.query
    page = pager(q)
    return render_template(
        'user/related.html',
        title=f"users following {user.handle}",
        user=user,
        page=page
    )


@bp.route('/<nickname>/edit', methods=('GET', 'POST'))
@this_user_required
@alert_on_exception(category=AssertionError)
def edit_profile(user):
    if request.method == 'POST':
        name = validate_text(request.form['name'])
        bio = validate_text(request.form['bio'])

        if validate_image(request.files['avatar']):
            avatar = (
                media
                .prepare_upload(request.files['avatar'])
                .transform(media.thumbnail_filter(_avatar_dim))
                .insert()
            )
            if avatar:
                user.avatar = avatar

        if name:
            user.name = name
        if bio:
            user.bio = bio
        db.session.commit()

        current_app.logger.info(f"user: {user.__dict__}")

        flash(f"saved profile for {user.handle}", category='alert-success')
        return redirect(url_for('user.posts', nickname=user.nickname))
    else:
        return render_template('user/edit.html', user=user)


@bp.route('/<nickname>/preferences', methods=['POST'])
@this_user_required
@alert_on_exception(category=AssertionError)
def preferences(user):
    user.info['theme'] = request.form.get('theme', preference('theme'))
    user.info['mute_video'] = request.form.get('muteVideo', None) is not None
    user.info['autoplay_video'] = request.form.get('autoplayVideo', None) is not None
    user.info['loop_video'] = request.form.get('loopVideo', None) is not None
    user.info['autoadvance_video'] = request.form.get('autoadvanceVideo', None) is not None
    user.info['mute_audio'] = request.form.get('muteAudio', None) is not None
    user.info['loop_audio'] = request.form.get('loopAudio', None) is not None
    user.info['autoadvance_audio'] = request.form.get('autoadvanceAudio', None) is not None

    user.info['hide_likes'] = request.form.get('hideLikes', None) is not None
    user.info['hide_cw'] = request.form.get('hideDescription', None) is not None

    unique_hidden_tags = set(request.form.get('hiddenTags', '').split())
    user.info['hidden_tags'] = [validate_tag(tag) for tag in unique_hidden_tags]

    flag_modified(user, 'info')
    db.session.commit()
    flash(f'updated preferences for {user.handle}', category='alert-success')
    return redirect(url_for('user.edit_profile', nickname=user.nickname))


@bp.route('/<nickname>/mod_nodes', methods=['GET', 'POST'])
@valid_user_required
@moderator_required
@alert_on_exception(category=AssertionError)
def mod_notes(user):
    if request.method == 'POST':
        user.info['mod_notes'] = validate_text(request.form.get('mod-notes', ''))

        flag_modified(user, 'info')
        db.session.commit()
        flash(f'updated mod notes for {user.handle}', category='alert-success')
        return redirect(url_for('user.posts', nickname=user.nickname))
    else:
        return render_template('user/edit_mod_notes.html', user=user)
