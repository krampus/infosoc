"""Flask app setup"""
from pathlib import Path
import bleach
import functools

from flask import Flask, send_from_directory, redirect, url_for, request, flash, g
from flask_misaka import Misaka
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from jinja2 import Markup


app = Flask(__package__, instance_relative_config=True)
Misaka(
    app,
    no_intra_emphasis=True,
    fenced_code=True,
    space_headers=True,
    tables=True,
    disable_indented_code=True,
)
# XXX is this safe?
app.jinja_env.filters['bleach'] = lambda txt: Markup(bleach.clean(
    txt,
    tags=bleach.ALLOWED_TAGS + ['p', 'pre', 'br'],
    strip=True
))


instance_path = Path(app.instance_path)
instance_path.mkdir(exist_ok=True, parents=True)

default_path = Path(__file__).parent.parent / 'default'
if not default_path.exists():
    app.logger.warn(
        f"Warning, could not find default path: {default_path}. Fallback site resources will be unavailable.")

npm_path = instance_path.parent / 'node_modules'


def instance_file(filename):
    file_path = instance_path / filename
    if not file_path.exists():
        app.logger.warn(f"Instance file not found: {file_path} (hint: copy from `default` and edit as needed)")
        file_path = default_path / filename
    return file_path


# Config
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
from .config import config, load  # noqa: E402
load(app)

# Database
db = SQLAlchemy(app)
is_sqlite = app.config['SQLALCHEMY_DATABASE_URI'].startswith('sqlite:')
migrate = Migrate(app, db, render_as_batch=is_sqlite)

# Other initialization
from . import formatter, cli  # noqa: E402
formatter.init_app(app)
cli.init_app(app)


def alert_on_exception(redirect_url=None, category=Exception):
    """Wrapper for view functions that should flash exceptions as errors to the user"""
    decorator_redirect_url = redirect_url

    def decorator(fn):
        @functools.wraps(fn)
        def inner(*args, redirect_url=None, **kwargs):
            try:
                return fn(*args, **kwargs)
            except category as e:
                app.logger.exception(e)
                flash(str(e), category='alert-danger')
                return redirect(redirect_url or decorator_redirect_url or request.environ['HTTP_REFERER'])
        return inner
    return decorator


# jinja globals
def jinja_global(fn):
    """Decorator to make a function visible as a jinja global"""
    app.jinja_env.globals[fn.__name__] = fn
    return fn


@jinja_global
def url_with_params(endpoint, kwargs={}):
    """Wraps flask.url_for by expanding `kwargs`"""
    return url_for(endpoint, **kwargs)


@jinja_global
def redirect_here(kwargs={}):
    """Redirect to the same page as the request with updated URL params"""
    params = dict(request.args.copy())
    params.update(request.view_args)
    params.update(kwargs)
    return url_for(request.endpoint, **params)


@jinja_global
def reveal_redirect(post):
    """Build the URL for a redirect to this page, forcing the given post to be shown"""
    return redirect_here(dict(
        reveal=' '.join(request.args.get('reveal', '').split() + [str(post.id)])
    ))


def post_revealed(post):
    """Has the given post been explicitly revealed?"""
    return str(post.id) in request.args.get('reveal', '').split()


@jinja_global
def hide_by_cw(post, user):
    """Should a post be hidden behind a content warning for the given user?"""
    if post and not post_revealed(post) and post.subject and post.description:
        return preference('hide_cw', user=user)
    else:
        return False


@jinja_global
def hidden_tags(post, user):
    """Return a list of tags that would cause this post to be hidden from the user."""
    if post and not post_revealed(post):
        hidden_tags = preference('hidden_tags', user=user)
        return [tag.name for tag in post.tags if tag.name in hidden_tags]


@jinja_global
def describe_page(page):
    """Return a human-readable description of a page, like '1-20 of 200'"""
    start = min((page.page - 1) * page.per_page + 1, page.total)
    end = min(start + page.per_page - 1, page.total)
    return f'{start}-{end} of {page.total}'


# fallback if a value is not found in user preferences, instance
# preferences, or default preferences
_default_preferences = dict(
    theme='default',
    mute_video=True,
    autoplay_video=True,
    loop_video=True,
    autoadvance_video=False,
    mute_audio=False,
    loop_audio=False,
    autoadvance_audio=True,
    hide_likes=False,
    hide_cw=True,
    hidden_tags=['nsfw', 'spoiler']
)


@jinja_global
def preference(key, user=None):
    """Get the value for a preference key, falling back to instance and application defaults.

    User-defined preferences are tried first. If the user is not
    logged in or has no setting for the preference, the instance
    default is tried. If no instance-default setting was given,
    finally we fall back to the application default preference defined
    in this file, raising a KeyError if it can't be found there
    either.
    """
    user = user or g.user
    if user and key in user.info:
        return user.info[key]
    elif key in config.preferences.__dict__:
        return config.preferences.__dict__[key]
    else:
        return _default_preferences[key]


# Views
from . import auth, user, post, about, index, tags, notification  # noqa: E402
app.register_blueprint(auth.bp)
app.register_blueprint(user.bp)
app.register_blueprint(post.bp)
app.register_blueprint(about.bp)
app.register_blueprint(index.bp)
app.register_blueprint(tags.bp)
app.register_blueprint(notification.bp)


@app.route('/')
def index():
    return redirect(url_for('index.results'))


@app.route('/media/<path:path>')
def media(path):
    return send_from_directory(str(app.config['MEDIA_PATH']), path)


@app.route('/lib/<path:path>')
def npm_lib(path):
    return send_from_directory(npm_path, path)


@app.route('/instance/<path:path>')
def instance(path):
    """Serve instance-relative site content, falling back to the default."""
    file_path = instance_path / 'site' / path
    if file_path.exists():
        return send_from_directory(instance_path / 'site', path)
    else:
        return send_from_directory(default_path / 'site', path)


# static root-level resources
@app.route('/favicon.ico')
def serve_favicon():
    return instance('favicon.png')


@app.route('/robots.txt')
def serve_robots():
    return instance('robots.txt')
