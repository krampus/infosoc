"""Tag views"""
from flask import Blueprint, render_template, request, jsonify, redirect, url_for

from .db.models import Tag, Post
from .app import db
from .index import paginated

bp = Blueprint('tags', __name__, url_prefix='/tags')

_SUGGEST_LIMIT_DEFAULT = 5


@bp.route('/')
@paginated
def index(pager):
    show = request.args.get('show', None)
    if show is None:
        return redirect(url_for('tags.index', show='popular'))
    if show == 'recent':
        subq = db.session.query(
            Tag,
            db.func.max(Post.updated_at).label('last_updated')
        ).outerjoin(Tag.posts).group_by(Tag).subquery()
        q = Tag.query.join(subq, subq.c.id == Tag.id).order_by(db.desc(subq.c.last_updated))
        title = 'recently used'
    else:  # popular
        q = Tag.query.order_by(db.desc(Tag.post_count))
        title = 'popular tags'

    page = pager(q)
    return render_template('tags/list.html', page=page, title=title)


@bp.route('/suggest.json')
def suggest():
    stub = request.args.get('stub', '')
    limit = request.args.get('limit', _SUGGEST_LIMIT_DEFAULT)

    res = (
        Tag.query
        .filter(Tag.name.like(f'{stub}%'))
        .order_by(db.desc(Tag.post_count))
        .limit(limit)
        .all()
    )
    return jsonify([t.name for t in res])
