"""Media upload and accessors"""
import hashlib
from functools import lru_cache
from io import BytesIO
from flask import current_app
from werkzeug.utils import secure_filename
from PIL import Image
from pathlib import Path
import subprocess
import requests

from .validation import validate_media
from .db.models import Media
from .db.mimetypes import MIMEType, ImageTypes, VideoTypes, AudioTypes
from .app import db

_thumbnail_dim = 512, 512  # TODO should this be configurable?


class RequestFromURL:
    """Mock file-store for content fetched from a remote URL"""
    def __init__(self, url):
        current_app.logger.info(f"Fetching media from {url}")
        self.filename = Path(url).name
        _request = requests.get(url, timeout=5, stream=True)
        self.headers = _request.headers

        # spool into iostream
        self.stream = BytesIO()
        maxsize = current_app.config['MAX_CONTENT_LENGTH']
        spooled = 0
        for chunk in _request.iter_content(4096):
            spooled += self.stream.write(chunk)
            if spooled > maxsize:
                _request.close()
                raise ValueError('Requested URL response too large')
        self.stream.seek(0)

    def save(self, path):
        # dump spooled iostream to disk
        self.stream.seek(0)
        with open(path, 'wb') as outfile:
            outfile.write(self.stream.read())


def thumbnail_filter(dim=_thumbnail_dim):
    """Image thumbnail filter factory"""
    def transform(img):
        img.thumbnail(dim)
        return img
    return transform


def md5sum_file(stream, bs=65536):
    # XXX is there any reason to use md5 vs sha1?
    hasher = hashlib.md5()
    stream.seek(0)
    buffer = stream.read(bs)
    while len(buffer) > 0:
        hasher.update(buffer)
        buffer = stream.read(bs)
    return hasher


def gif_metadata(gif):
    # TODO find a library to do this
    frame_durations = []  # in ms
    for frame_i in range(gif.n_frames):
        gif.seek(frame_i)
        frame_durations.append(gif.info.get('duration', 0))
    duration = sum(frame_durations) / 1000
    framerate = (gif.n_frames / duration) if duration > 0 else None
    return duration, framerate


def video_metadata(f_path):
    # TODO find a library to do this
    p_out = subprocess.check_output([
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'v:0',
        '-show_entries', 'stream=width,height,r_frame_rate',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        str(f_path)
    ])
    raw_w, raw_h, raw_framerate = p_out.splitlines()
    frames, time = raw_framerate.split(b'/')

    return int(raw_w), int(raw_h), float(frames) / float(time)


def media_duration(f_path):
    # TODO find a library to do this
    p_out = subprocess.check_output([
        'ffprobe',
        '-v', 'error',
        '-show_entries', 'format=duration',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        str(f_path)
    ])
    return float(p_out)


def prepare_upload(file_store):
    file_store = validate_media(file_store)
    if file_store is not None:
        mime_type = file_store.headers['Content-Type']
        if mime_type in [t.value for t in ImageTypes] and mime_type != ImageTypes.SVG.value:
            # PIL can't handle SVGs so we'll upload them generically
            return ImagePrep(file_store)
        elif mime_type in [t.value for t in VideoTypes]:
            return VideoPrep(file_store)
        elif mime_type in [t.value for t in AudioTypes]:
            return AudioPrep(file_store)
        else:
            return UploadPrep(file_store)


class UploadPrep:
    def __init__(self, file_store):
        self._file = file_store

    def thumbnail(self):
        """Return an ImagePrep for a thumbnail, if one can be made"""
        return None

    @property
    @lru_cache(1)
    def _filename(self):
        return secure_filename(self._file.filename)

    @property
    @lru_cache(1)
    def _mime_type(self):
        return MIMEType(self._file.headers['Content-Type'])

    @property
    @lru_cache(1)
    def _path(self):
        return self._md5sum + Path(self._filename).suffix

    @property
    @lru_cache(1)
    def _disk_path(self):
        return current_app.config['MEDIA_PATH'] / self._path

    @property
    @lru_cache(1)
    def _md5sum(self):
        return md5sum_file(self._file.stream).hexdigest()

    @property
    @lru_cache(1)
    def _info(self):
        # must be called AFTER self._write
        assert self._disk_path.exists(), "missing storage file"

        return dict(
            size_bytes=self._disk_path.stat().st_size
        )

    def _write(self):
        self._file.stream.seek(0)
        self._file.save(str(self._disk_path))

    def insert(self):
        current_app.logger.info(f"Writing {self._filename} to {self._disk_path}")
        self._write()

        media = Media(
            filename=self._filename,
            mime_type=self._mime_type,
            path=self._path,
            md5sum=self._md5sum,
            info=self._info
        )
        db.session.add(media)
        return media


class ImagePrep(UploadPrep):
    def __init__(self, file_store):
        super().__init__(file_store)
        self._file.stream.seek(0)
        self._img = Image.open(self._file.stream)
        self._transformed = False

    def transform(self, transformation):
        self._img = transformation(self._img)
        self._transformed = True
        return self

    def thumbnail(self):
        thumbnail_prep = ImagePrep(self._file)
        thumbnail_prep.transform(thumbnail_filter(dim=_thumbnail_dim))
        return thumbnail_prep

    @property
    @lru_cache(1)
    def _md5sum(self):
        if self._transformed:
            iostream = BytesIO()
            # needs arbitrary format or Pillow will complain
            self._img.save(iostream, format='GIF')
            return md5sum_file(iostream).hexdigest()
        else:
            return super()._md5sum

    @property
    @lru_cache(1)
    def _info(self):
        w, h = self._img.size
        info = super()._info
        info['width_px'], info['height_px'] = self._img.size

        if self._img.format == 'GIF' and self._img.n_frames > 1:
            info['length_s'], info['framerate_fps'] = gif_metadata(self._img)

        return info

    def _write(self):
        if self._transformed:
            self._img.save(str(self._disk_path))
        else:
            super()._write()


class VideoPrep(UploadPrep):
    @property
    @lru_cache(1)
    def _info(self):
        info = super()._info
        try:
            info['width_px'], info['height_px'], info['framerate_fps'] = video_metadata(self._disk_path)
        except Exception as e:
            current_app.logger.exception(e)
            current_app.logger.error("Could not build video metadata")
        try:
            info['length_s'] = media_duration(self._disk_path)
        except Exception as e:
            current_app.logger.exception(e)
            current_app.logger.error("Could not get video length")

        return info


class AudioPrep(UploadPrep):
    @property
    @lru_cache(1)
    def _info(self):
        info = super()._info
        try:
            info['length_s'] = media_duration(self._disk_path)
        except Exception as e:
            current_app.logger.exception(e)
            current_app.logger.error("Could not get audio length")

        return info
