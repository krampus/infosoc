"""added reply_to_id fk to posts table

Revision ID: 85385e246cef
Revises: aacab7d98321
Create Date: 2019-08-03 18:18:33.204501

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '85385e246cef'
down_revision = 'aacab7d98321'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('posts', sa.Column('reply_to_id', sa.Integer, sa.ForeignKey('posts.id')))


def downgrade():
    op.drop_column('posts', 'reply_to_id')
