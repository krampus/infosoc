"""media has path

Revision ID: e3505a7b210b
Revises: 48e8ff32ce1d
Create Date: 2019-08-10 22:40:20.035289

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

from enum import Enum


# revision identifiers, used by Alembic.
revision = 'e3505a7b210b'
down_revision = '48e8ff32ce1d'
branch_labels = None
depends_on = None


class MIMEType(Enum):
    BMP = 'image/bmp'
    GIF = 'image/gif'
    ICO = 'image/vnd.microsoft.icon'
    JPEG = 'image/jpeg'
    PNG = 'image/png'
    SVG = 'image/svg+xml'
    TIFF = 'image/tiff'
    WEBP = 'image/webp'
    AVI = 'video/x-msvideo'
    MPEG = 'video/mpeg'
    OGV = 'video/ogg'
    TS = 'video/mp2t'
    WEBM = 'video/webm'
    BZ = 'application/x-bzip'
    BZ2 = 'application/x-bzip2'
    JAR = 'application/java-archive'
    RAR = 'application/x-rar-compressed'
    TAR = 'application/x-tar'
    ZIP = 'application/zip'
    BIN = 'application/octet-stream'
    AZW = 'application/vnd.amazon.ebook'
    DOC = 'application/msword'
    DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    EPUB = 'application/epub+zip'
    HTML = 'text/html'
    TXT = 'text/plain'
    JS = 'text/javascript'
    JSON = 'application/json'
    ODP = 'application/vnd.oasis.opendocument.presentation'
    ODS = 'application/vnd.oasis.opendocument.spreadsheet'
    ODT = 'application/vnd.oasis.opendocument.text'
    SWF = 'application/x-shockwave-flash'


Base = declarative_base()


class Media(Base):
    __tablename__ = 'media'
    id = sa.Column(sa.Integer, primary_key=True)
    mime_type = sa.Column(sa.Enum(MIMEType), nullable=False)
    path = sa.Column(sa.UnicodeText)


def upgrade():
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    op.add_column('media', sa.Column('path', sa.UnicodeText()))
    for row in session.query(Media):
        row.path = f"{row.id}.{row.mime_type.name}"
    session.commit()

    with op.batch_alter_table('media', schema=None) as batch_op:
        batch_op.alter_column('path', nullable=False)


def downgrade():
    with op.batch_alter_table('media', schema=None) as batch_op:
        batch_op.drop_column('path')
