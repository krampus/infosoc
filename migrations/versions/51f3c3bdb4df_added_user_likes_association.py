"""added user_likes association

Revision ID: 51f3c3bdb4df
Revises: f1114c757308
Create Date: 2019-08-09 14:23:24.817831

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '51f3c3bdb4df'
down_revision = 'f1114c757308'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user_likes',
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('post_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.ForeignKeyConstraint(['post_id'], ['posts.id'], )
    )


def downgrade():
    op.drop_table('user_likes')
