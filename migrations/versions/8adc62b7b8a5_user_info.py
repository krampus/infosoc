"""user info

Revision ID: 8adc62b7b8a5
Revises: 85385e246cef
Create Date: 2019-08-06 12:36:13.965683

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import sqlite
from sqlalchemy.sql import table

# revision identifiers, used by Alembic.
revision = '8adc62b7b8a5'
down_revision = '85385e246cef'
branch_labels = None
depends_on = None


def upgrade_data():
    users = table(
        'users',
        sa.Column('info', sa.JSON)
    )
    op.execute(
        users
        .update()
        .where(users.c.info == None)
        .values({
            'info': dict(
                blocks=[],
                deactivated=False,
                hidden_tags=['nsfw'],
                mod_notes='',
                moderator=False,
            )
        })
    )


def upgrade():
    upgrade_data()

    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.alter_column('info',
                              existing_type=sqlite.JSON(),
                              nullable=False)


def downgrade():
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.alter_column('info',
                              existing_type=sqlite.JSON(),
                              nullable=True)
