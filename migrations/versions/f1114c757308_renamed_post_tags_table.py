"""renamed post_tags table

Revision ID: f1114c757308
Revises: 8adc62b7b8a5
Create Date: 2019-08-09 13:52:02.951633

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f1114c757308'
down_revision = '8adc62b7b8a5'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('association', 'post_tags')


def downgrade():
    op.rename_table('post_tags', 'association')
