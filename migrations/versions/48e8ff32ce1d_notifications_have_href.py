"""notifications have href

Revision ID: 48e8ff32ce1d
Revises: 23ea96dfb654
Create Date: 2019-08-09 18:31:17.157186

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '48e8ff32ce1d'
down_revision = '23ea96dfb654'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('notifications', schema=None) as batch_op:
        batch_op.add_column(sa.Column('href', sa.UnicodeText(), nullable=True))


def downgrade():
    with op.batch_alter_table('notifications', schema=None) as batch_op:
        batch_op.drop_column('href')
