"""Added media metadata

Revision ID: 80c3b1fa120d
Revises: 1200981bb58b
Create Date: 2019-08-13 11:19:44.008113

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base

import hashlib
from pathlib import Path
from enum import Enum
from PIL import Image
import subprocess


# revision identifiers, used by Alembic.
revision = '80c3b1fa120d'
down_revision = '1200981bb58b'
branch_labels = None
depends_on = None


# XXX I think this needs to be hardcoded. Change if needed, I guess?
MEDIA_PATH = Path('instance/media')


class MIMEType(Enum):
    BMP = 'image/bmp'
    GIF = 'image/gif'
    ICO = 'image/vnd.microsoft.icon'
    JPEG = 'image/jpeg'
    PNG = 'image/png'
    SVG = 'image/svg+xml'
    TIFF = 'image/tiff'
    WEBP = 'image/webp'
    AVI = 'video/x-msvideo'
    MPEG = 'video/mpeg'
    MP4 = 'video/mp4'
    OGV = 'video/ogg'
    TS = 'video/mp2t'
    WEBM = 'video/webm'
    MIDI = 'audio/midi'
    MID = 'audio/x-midi'
    MP3 = 'audio/mp3'
    OGA = 'audio/ogg'
    WAV = 'audio/wav'
    WEBA = 'audio/webm'
    BZ = 'application/x-bzip'
    BZ2 = 'application/x-bzip2'
    JAR = 'application/java-archive'
    RAR = 'application/x-rar-compressed'
    TAR = 'application/x-tar'
    ZIP = 'application/zip'
    BIN = 'application/octet-stream'
    AZW = 'application/vnd.amazon.ebook'
    DOC = 'application/msword'
    DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    EPUB = 'application/epub+zip'
    HTML = 'text/html'
    TXT = 'text/plain'
    MD = 'text/markdown'
    JS = 'text/javascript'
    JSON = 'application/json'
    ODP = 'application/vnd.oasis.opendocument.presentation'
    ODS = 'application/vnd.oasis.opendocument.spreadsheet'
    ODT = 'application/vnd.oasis.opendocument.text'
    SWF = 'application/x-shockwave-flash'


image_types = [t for t in MIMEType if t.value.startswith('image/')]

video_types = [t for t in MIMEType if t.value.startswith('video/')]

audio_types = [t for t in MIMEType if t.value.startswith('audio/')]


Base = declarative_base()


class Media(Base):
    __tablename__ = 'media'
    id = sa.Column(sa.Integer, primary_key=True)
    mime_type = sa.Column(sa.Enum(MIMEType), nullable=False)
    path = sa.Column(sa.UnicodeText)
    md5sum = sa.Column(sa.UnicodeText, nullable=False)
    info = sa.Column(sa.JSON, nullable=False)


def md5sum_file(stream, bs=65536):
    hasher = hashlib.md5()
    stream.seek(0)
    buffer = stream.read(bs)
    while len(buffer) > 0:
        hasher.update(buffer)
        buffer = stream.read(bs)
    return hasher


def gif_metadata(gif):
    frame_durations = []  # in ms
    for frame_i in range(gif.n_frames):
        gif.seek(frame_i)
        frame_durations.append(gif.info.get('duration', 0))
    duration = sum(frame_durations) / 1000
    framerate = (gif.n_frames / duration) if duration > 0 else None
    return duration, framerate


def video_dimensions(f_path):
    p_out = subprocess.check_output([
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'v:0',
        '-show_entries', 'stream=width,height',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        str(f_path)
    ])
    raw_w, raw_h = p_out.splitlines()
    return int(raw_w), int(raw_h)


def media_duration(f_path):
    p_out = subprocess.check_output([
        'ffprobe',
        '-v', 'error',
        '-show_entries', 'format=duration',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        str(f_path)
    ])
    return float(p_out)


def upgrade_data(session):
    for row in session.query(Media):
        file_path = MEDIA_PATH / row.path
        if not file_path.exists():
            continue
        print(f"Upgrading {file_path}")

        info = dict(
            size_bytes=file_path.stat().st_size
        )
        with open(file_path, 'rb') as f:
            row.md5sum = md5sum_file(f).hexdigest()

        try:
            if row.mime_type in image_types:
                img = Image.open(file_path)
                w, h = img.size
                info.update(
                    height_px=h,
                    width_px=w
                )
                if img.format == 'GIF' and img.n_frames > 1:
                    length, framerate = gif_metadata(img)
                    info.update(
                        length_s=length,
                        framerate_fps=framerate
                    )
            if row.mime_type in video_types:
                w, h = video_dimensions(file_path)
                info.update(
                    height_px=h,
                    width_px=w
                )
            if row.mime_type in audio_types + video_types:
                info.update(
                    length_s=media_duration(file_path)
                )
        except Exception as ex:
            print(f"Error: {ex}")
        finally:
            row.info = info


def upgrade():
    with op.batch_alter_table('media', schema=None) as batch_op:
        batch_op.add_column(sa.Column('md5sum', sa.UnicodeText, nullable=True))
        batch_op.add_column(sa.Column('info', sa.JSON, nullable=True))

    bind = op.get_bind()
    session = orm.Session(bind=bind)
    upgrade_data(session)
    session.commit()

    with op.batch_alter_table('media', schema=None) as batch_op:
        batch_op.alter_column('md5sum', nullable=False)
        batch_op.alter_column('info', nullable=False)


def downgrade():
    with op.batch_alter_table('media', schema=None) as batch_op:
        batch_op.drop_column('md5sum')
        batch_op.drop_column('info')
