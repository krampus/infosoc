"""users and posts can be deactivated

Revision ID: 1200981bb58b
Revises: e3505a7b210b
Create Date: 2019-08-11 15:40:32.571818

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1200981bb58b'
down_revision = 'e3505a7b210b'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.add_column(sa.Column('removed', sa.Boolean(), default=False, nullable=False))

    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.add_column(sa.Column('deactivated', sa.Boolean(), default=False, nullable=False))



def downgrade():
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.drop_column('deactivated')

    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.drop_column('removed')
