"""added post reply_to_id

Revision ID: 23ea96dfb654
Revises: 1ac631c6f76d
Create Date: 2019-08-09 16:30:17.276840

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '23ea96dfb654'
down_revision = '1ac631c6f76d'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.create_foreign_key('fk_reply_to_id', 'posts', ['reply_to_id'], ['id'])


def downgrade():
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.drop_constraint('fk_reply_to_id', type_='foreignkey')
