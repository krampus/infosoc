"""Initial migration

Revision ID: aacab7d98321
Revises:
Create Date: 2019-07-31 13:18:49.998934

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aacab7d98321'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'association',
        sa.Column('post_id', sa.Integer(), nullable=True),
        sa.Column('tag_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['post_id'], ['posts.id'], ),
        sa.ForeignKeyConstraint(['tag_id'], ['tags.id'], )
    )
    op.create_table(
        'media',
        sa.Column('inserted_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('filename', sa.UnicodeText(), nullable=False),
        sa.Column('mime_type', sa.Enum(
            'BMP', 'GIF', 'ICO', 'JPEG', 'PNG', 'SVG', 'TIFF', 'WEBP',
            'AVI', 'MPEG', 'OGV', 'TS', 'WEBM', 'BZ', 'BZ2', 'JAR',
            'RAR', 'TAR', 'ZIP', 'BIN', 'AZW', 'DOC', 'DOCX', 'EPUB',
            'HTML', 'JS', 'JSON', 'ODP', 'ODS', 'ODT', 'SWF',
            name='mimetype'
        ), nullable=False),
        sa.Column('post_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['post_id'], ['posts.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'posts',
        sa.Column('inserted_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('author_id', sa.Integer(), nullable=True),
        sa.Column('thumbnail_id', sa.Integer(), nullable=True),
        sa.Column('subject', sa.UnicodeText(), nullable=True),
        sa.Column('description', sa.UnicodeText(), nullable=True),
        sa.ForeignKeyConstraint(['author_id'], ['users.id'], ),
        sa.ForeignKeyConstraint(['thumbnail_id'], ['media.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_table(
        'tags',
        sa.Column('inserted_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.UnicodeText(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('name')
    )
    op.create_table(
        'users',
        sa.Column('inserted_at', sa.DateTime(), nullable=True),
        sa.Column('updated_at', sa.DateTime(), nullable=True),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('email', sa.UnicodeText(), nullable=True),
        sa.Column('password_hash', sa.UnicodeText(), nullable=False),
        sa.Column('nickname', sa.UnicodeText(), nullable=False),
        sa.Column('name', sa.UnicodeText(), nullable=True),
        sa.Column('bio', sa.UnicodeText(), nullable=True),
        sa.Column('info', sa.JSON(), nullable=True),
        sa.Column('avatar_id', sa.Integer(), nullable=True),
        sa.Column('local', sa.Boolean(), nullable=False),
        sa.Column('following', sa.JSON(), nullable=False),
        sa.Column('ap_id', sa.UnicodeText(), nullable=False),
        sa.Column('follower_address', sa.UnicodeText(), nullable=False),
        sa.ForeignKeyConstraint(['avatar_id'], ['media.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('users')
    op.drop_table('tags')
    op.drop_table('posts')
    op.drop_table('media')
    op.drop_table('association')
