"""Unit tests covering infosoc.formatter"""
from flask import current_app
from unittest import TestCase
from jinja2 import Markup

from infosoc import formatter

from . import WithAppContext


# XXX man that config module makes testing real hard, doesn't it?
class TestRender(WithAppContext, TestCase):
    def setUp(self):
        super().setUp()
        self.host = current_app.config['SERVER_NAME']

    def test_render_produces_markup(self):
        self.assertIsInstance(formatter.render_all("#tag"), Markup)

    def test_render_tags(self):
        text = "#tag"
        expected = f'<a class="tag-link" href="http://{self.host}/posts/?query=tag">#tag</a>'
        self.assertEqual(expected, formatter.render_all(text))

        text = "#tags at the start, in the #middle, and at the #end"
        expected = f'<a class="tag-link" href="http://{self.host}/posts/?query=tags">#tags</a> at the start, in the <a class="tag-link" href="http://{self.host}/posts/?query=middle">#middle</a>, and at the <a class="tag-link" href="http://{self.host}/posts/?query=end">#end</a>'
        self.assertEqual(expected, formatter.render_all(text))

    def test_render_local_mentions(self):
        text = "@guy"
        expected = f'<a class="mention-link" href="http://{self.host}/user/guy/posts">@guy</a>'
        self.assertEqual(expected, formatter.render_all(text))

        text = f"@guy@{self.host}"
        expected = f'<a class="mention-link" href="http://{self.host}/user/guy/posts">@guy@{self.host}</a>'
        self.assertEqual(expected, formatter.render_all(text))

        text = f"@mentions at the start, in the @middle@{self.host}, and at the @end"
        expected = f'<a class="mention-link" href="http://{self.host}/user/mentions/posts">@mentions</a> at the start, in the <a class="mention-link" href="http://{self.host}/user/middle/posts">@middle@{self.host}</a>, and at the <a class="mention-link" href="http://{self.host}/user/end/posts">@end</a>'
        self.assertEqual(expected, formatter.render_all(text))

    def test_render_federated_mentions(self):
        self.skipTest("TODO -- implement. I have no idea what this looks like.")

    def test_render_mixed_content(self):
        text = f"hey @guy check out this #cool_stuff I found in the #dumpster_in_the_alley with @rowdyroddypiper@{self.host}"
        expected = f'hey <a class="mention-link" href="http://{self.host}/user/guy/posts">@guy</a> check out this <a class="tag-link" href="http://{self.host}/posts/?query=cool_stuff">#cool_stuff</a> I found in the <a class="tag-link" href="http://{self.host}/posts/?query=dumpster_in_the_alley">#dumpster_in_the_alley</a> with <a class="mention-link" href="http://{self.host}/user/rowdyroddypiper/posts">@rowdyroddypiper@{self.host}</a>'
        self.assertEqual(expected, formatter.render_all(text))
