"""Unit tests covering infosoc.query.parser"""
from unittest import TestCase
from types import SimpleNamespace
from pyparsing import ParseException

from infosoc.query.parser import Tag, AndOp, OrOp, XorOp, NotOp, Parser, MetaTag, NamedTag
from infosoc.db.models import Post

from .. import WithDB
from ..fixture_builder import make_tag, make_post


class MyMetaTag(MetaTag):
    key = 'meta'
    values = ['a', 'b']

    def filter_query(self, qb):
        pass


class MyNamedTag(NamedTag):
    key = 'named'


class TestParser(TestCase):
    def setUp(self):
        super().setUp()
        self.parser = Parser()

    def _parse_test(self, query, expected_tree, expected_metatags=[]):
        qb = self.parser.parse_query(query)
        self.assertEqual(
            qb.tree, expected_tree,
            f"Parse tree mismatch: expected {expected_tree._tree() if expected_tree else None}, got {qb.tree._tree() if qb.tree else None}"
        )
        for mt, expected in zip(qb.metafilters, expected_metatags):
            self.assertEqual(mt, expected)

    def test_parse_tag(self):
        self._parse_test(
            "my_tag",
            Tag(["my_tag"])
        )

        self._parse_test(
            "  my_tag    ",
            Tag(["my_tag"])
        )

        self._parse_test(
            "  my-weird-tag    ",
            Tag(["my-weird-tag"])
        )

        self._parse_test(
            "unused_key:value",
            Tag(["unused_key:value"])
        )

    def test_parse_not(self):
        self._parse_test(
            "-my_tag",
            NotOp([[
                Tag(['my_tag'])
            ]])
        )

        self._parse_test(
            "--my_tag",
            NotOp([[
                NotOp([[
                    Tag(['my_tag'])
                ]])
            ]])
        )

    def test_parse_and(self):
        self._parse_test(
            "tag1 tag2",
            AndOp([[
                Tag(['tag1']),
                Tag(['tag2'])
            ]])
        )

        self._parse_test(
            "tag1 tag2 tag3",
            AndOp([[
                Tag(['tag1']),
                AndOp([[
                    Tag(['tag2']),
                    Tag(['tag3'])
                ]])
            ]])
        )

    def test_parse_or(self):
        self._parse_test(
            "tag1, tag2",
            OrOp([[
                Tag(['tag1']),
                Tag(['tag2'])
            ]])
        )

        self._parse_test(
            "tag1,tag2,tag3",
            OrOp([[
                Tag(['tag1']),
                OrOp([[
                    Tag(['tag2']),
                    Tag(['tag3'])
                ]])
            ]])
        )

    def test_parse_xor(self):
        self._parse_test(
            "tag1 ^ tag2",
            XorOp([[
                Tag(['tag1']),
                Tag(['tag2'])
            ]])
        )

        self._parse_test(
            "tag1^tag2^tag3",
            XorOp([[
                Tag(['tag1']),
                XorOp([[
                    Tag(['tag2']),
                    Tag(['tag3'])
                ]])
            ]])
        )

    def test_parse_metatags(self):
        self._parse_test(
            "meta:a",
            None,
            [
                MyMetaTag(['a'])
            ]
        )

        self._parse_test(
            "tag1 tag2 meta:b",
            AndOp([[
                Tag(['tag1']),
                Tag(['tag2'])
            ]]),
            [
                MyMetaTag(['b'])
            ]
        )

        self._parse_test(
            "meta:b tag1 tag2",
            AndOp([[
                Tag(['tag1']),
                Tag(['tag2'])
            ]]),
            [
                MyMetaTag(['b'])
            ]
        )

    def test_parse_raises_on_non_root_metatag(self):
        with self.assertRaises(ParseException):
            self.parser.parse_query('tag1 meta:a tag2')

        with self.assertRaises(ParseException):
            self.parser.parse_query('tag1 tag2, meta:a')

        with self.assertRaises(ParseException):
            self.parser.parse_query('-meta:a')

        with self.assertRaises(ParseException):
            self.parser.parse_query('(meta:a tag1) tag2')

    def test_parse_raises_on_unknown_metatag_value(self):
        with self.assertRaises(ParseException):
            self.parser.parse_query('meta:unknown')

    def test_parse_named_tags(self):
        self._parse_test(
            "named:thing",
            MyNamedTag([Tag(['thing'])])
        )

        self._parse_test(
            "named:thing tag1 tag2",
            AndOp([[
                MyNamedTag([Tag(['thing'])]),
                AndOp([[
                    Tag(['tag1']),
                    Tag(['tag2'])
                ]])
            ]])
        )

        self._parse_test(
            "tag1 tag2 named:thing",
            AndOp([[
                Tag(['tag1']),
                AndOp([[
                    Tag(['tag2']),
                    MyNamedTag([Tag(['thing'])])
                ]])
            ]])
        )

    def test_parse_kitchen_sink(self):
        # XXX if you find an interesting parse case, throw it in here
        self._parse_test(
            "tag1 -tag2",
            AndOp([[
                Tag(['tag1']),
                NotOp([[
                    Tag(['tag2'])
                ]])
            ]])
        )

        self._parse_test(
            "-(tag1 tag2)",
            NotOp([[
                AndOp([[
                    Tag(['tag1']),
                    Tag(['tag2'])
                ]])
            ]])
        )

        self._parse_test(
            "tag1,-tag2 tag3",
            AndOp([[
                OrOp([[
                    Tag(['tag1']),
                    NotOp([[
                        Tag(['tag2'])
                    ]])
                ]]),
                Tag(['tag3'])
            ]])
        )

        self._parse_test(
            "tag1,-(tag2 tag3)",
            OrOp([[
                Tag(['tag1']),
                NotOp([[
                    AndOp([[
                        Tag(['tag2']),
                        Tag(['tag3'])
                    ]])
                ]])
            ]])
        )

        self._parse_test(
            "-tag1, tag2 tag3 ^ -tag4",
            AndOp([[
                OrOp([[
                    NotOp([[
                        Tag(['tag1'])
                    ]]),
                    Tag(['tag2'])
                ]]),
                XorOp([[
                    Tag(['tag3']),
                    NotOp([[
                        Tag(['tag4'])
                    ]])
                ]])
            ]])
        )

        self._parse_test(
            "-tag1, (tag2 tag3) ^ -tag4",
            XorOp([[
                OrOp([[
                    NotOp([[
                        Tag(['tag1'])
                    ]]),
                    AndOp([[
                        Tag(['tag2']),
                        Tag(['tag3'])
                    ]])
                ]]),
                NotOp([[
                    Tag(['tag4'])
                ]])
            ]])
        )


class TestBuildQuery(WithDB, TestCase):
    def setUp(self):
        super().setUp()
        tags = SimpleNamespace(
            a=make_tag(name='tag_a'),
            b=make_tag(name='tag_b'),
            c=make_tag(name='tag_c'),
            d=make_tag(name='tag_d')
        )
        self.tags = tags

        self.posts = SimpleNamespace(
            a=make_post(tags=[tags.a]),
            b=make_post(tags=[tags.b]),
            c=make_post(tags=[tags.c]),
            d=make_post(tags=[tags.d]),
            abc=make_post(tags=[tags.a, tags.b, tags.c]),
            abd=make_post(tags=[tags.a, tags.b, tags.d]),
            acd=make_post(tags=[tags.a, tags.c, tags.d]),
            bcd=make_post(tags=[tags.b, tags.c, tags.d]),
            abcd=make_post(tags=[tags.a, tags.b, tags.c, tags.d]),
        )

    def _query_test(self, tree, expected):
        results = Post.query.filter(tree.build_query()).all()
        self.assertSetEqual(set(results), expected)

    def test_query_tag(self):
        self._query_test(
            Tag(['tag_a']),
            {self.posts.a, self.posts.abc, self.posts.abd,
             self.posts.acd, self.posts.abcd}
        )

    def test_query_not(self):
        self._query_test(
            NotOp([[
                Tag(['tag_a'])
            ]]),
            {self.posts.b, self.posts.c, self.posts.d, self.posts.bcd}
        )

        self._query_test(
            NotOp([[
                NotOp([[
                    Tag(['tag_a'])
                ]]),
            ]]),
            {self.posts.a, self.posts.abc, self.posts.abd,
             self.posts.acd, self.posts.abcd}
        )

    def test_query_and(self):
        self._query_test(
            AndOp([[
                Tag(['tag_a']),
                Tag(['tag_b'])
            ]]),
            {self.posts.abc, self.posts.abd, self.posts.abcd}
        )

        self._query_test(
            AndOp([[
                AndOp([[
                    Tag(['tag_a']),
                    Tag(['tag_b']),
                ]]),
                Tag(['tag_c'])
            ]]),
            {self.posts.abc, self.posts.abcd}
        )

    def test_query_or(self):
        self._query_test(
            OrOp([[
                Tag(['tag_a']),
                Tag(['tag_b'])
            ]]),
            {self.posts.a, self.posts.b, self.posts.abc,
             self.posts.abd, self.posts.acd, self.posts.bcd,
             self.posts.abcd}
        )

        self._query_test(
            OrOp([[
                OrOp([[
                    Tag(['tag_a']),
                    Tag(['tag_b']),
                ]]),
                Tag(['tag_c'])
            ]]),
            {self.posts.a, self.posts.b, self.posts.c, self.posts.abc,
             self.posts.abd, self.posts.acd, self.posts.bcd,
             self.posts.abcd}
        )

    def test_query_xor(self):
        self._query_test(
            XorOp([[
                Tag(['tag_a']),
                Tag(['tag_b'])
            ]]),
            {self.posts.a, self.posts.b, self.posts.acd, self.posts.bcd}
        )

        self._query_test(
            XorOp([[
                XorOp([[
                    Tag(['tag_a']),
                    Tag(['tag_b']),
                ]]),
                Tag(['tag_c'])
            ]]),
            {self.posts.a, self.posts.b, self.posts.c, self.posts.abc, self.posts.abcd}
        )

    def test_query_kitchen_sink(self):
        # XXX if you find an interesting query case, throw it in here
        self._query_test(
            AndOp([[
                Tag(['tag_a']),
                NotOp([[
                    Tag(['tag_b'])
                ]])
            ]]),
            {self.posts.a, self.posts.acd}
        )

        self._query_test(
            NotOp([[
                AndOp([[
                    Tag(['tag_a']),
                    Tag(['tag_b'])
                ]])
            ]]),
            {self.posts.a, self.posts.b, self.posts.c, self.posts.d,
             self.posts.acd, self.posts.bcd}
        )

        self._query_test(
            AndOp([[
                OrOp([[
                    Tag(['tag_a']),
                    NotOp([[
                        Tag(['tag_b'])
                    ]])
                ]]),
                Tag(['tag_c'])
            ]]),
            {self.posts.c, self.posts.abc, self.posts.acd, self.posts.abcd}
        )

        self._query_test(
            OrOp([[
                Tag(['tag_a']),
                NotOp([[
                    AndOp([[
                        Tag(['tag_b']),
                        Tag(['tag_c'])
                    ]])
                ]])
            ]]),
            {self.posts.a, self.posts.b, self.posts.c, self.posts.d,
             self.posts.abc, self.posts.abd, self.posts.acd,
             self.posts.abcd}
        )
