"""Unit tests covering infosoc.query.special_tags"""
from unittest import TestCase
from types import SimpleNamespace
from datetime import datetime

from infosoc.query.special_tags import (
    SortTag, AuthorTag, SubjectTag, DescriptionTag, MediaCategoryTag, MediaMIMETypeTag, ReplyTag
)
from infosoc.query.parser import AndOp, NotOp, Tag
from infosoc.query.builder import QueryBuilder
from infosoc.db.mimetypes import MIMEType

from .. import WithDB
from ..fixture_builder import make_tag, make_user, make_post, make_media


def dt(timestamp):
    return datetime.fromtimestamp(timestamp)


class QueryMixin:
    def _query_test(self, tokens, expected):
        qb = QueryBuilder()
        qb.add_tokens(tokens)
        self.assertSetEqual(set(qb.build().all()), set(expected))

    def _query_test_ordered(self, tokens, expected):
        qb = QueryBuilder()
        qb.add_tokens(tokens)
        self.assertListEqual(qb.build().all(), expected)


class TestSortTag(WithDB, QueryMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.tags = SimpleNamespace(
            a=make_tag(name='a')
        )

        self.posts = [
            make_post(inserted_at=dt(1), updated_at=dt(8), tags=[self.tags.a]),
            make_post(inserted_at=dt(2), updated_at=dt(7), tags=[self.tags.a]),
            make_post(inserted_at=dt(3), updated_at=dt(6)),
            make_post(inserted_at=dt(4), updated_at=dt(5))
        ]

    def test_sort_by_date(self):
        self._query_test_ordered(
            [SortTag(['date'])],
            self.posts[::-1]
        )

        self._query_test_ordered(
            [SortTag(['date_desc'])],
            self.posts[::-1]
        )

        self._query_test_ordered(
            [SortTag(['date_asc'])],
            self.posts
        )

        self._query_test_ordered(
            [
                Tag(['a']),
                SortTag(['date'])
            ],
            self.posts[:2][::-1]
        )

    def test_sort_by_edited(self):
        self._query_test_ordered(
            [SortTag(['edited'])],
            self.posts
        )

        self._query_test_ordered(
            [SortTag(['edited_desc'])],
            self.posts
        )

        self._query_test_ordered(
            [SortTag(['edited_asc'])],
            self.posts[::-1]
        )

        self._query_test_ordered(
            [
                Tag(['a']),
                SortTag(['edited'])
            ],
            self.posts[:2]
        )


class TestNamedTags(WithDB, QueryMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.users = SimpleNamespace(
            guy=make_user(nickname='guy', name='Guy Debord'),
            lain=make_user(nickname='lain', name='Iwakura Lain')
        )
        self.tags = SimpleNamespace(
            a=make_tag(name='a'),
            b=make_tag(name='b')
        )
        self.posts = SimpleNamespace(
            guy1=make_post(
                author=self.users.guy,
                subject="guy's first post",
                description="why, hello there!",
                tags=[self.tags.b],
            ),
            guy2=make_post(
                author=self.users.guy,
                subject="guy's second post",
                description="Plagiarism is neccessary. Progress implies it.",
                tags=[self.tags.a, self.tags.b],
            ),
            lain1=make_post(
                author=self.users.lain,
                subject="lain's first post",
                description="hello!",
            ),
            lain2=make_post(
                author=self.users.lain,
                subject="lain's second post",
                description="Let's all love lain!",
                tags=[self.tags.a],
            ),
            no_author=make_post(
                subject="mystery post",
                description="Guess I have no author?",
            )
        )

    def test_author_tag(self):
        self._query_test(
            [AuthorTag(['guy'])],
            [self.posts.guy1, self.posts.guy2]
        )

        self._query_test(
            [AuthorTag(['who_is_this_clown'])],
            []
        )

    def test_negate_author_tag(self):
        self._query_test(
            [NotOp([[
                AuthorTag(['guy'])
            ]])],
            [self.posts.lain1, self.posts.lain2, self.posts.no_author]
        )

    def test_mix_author_tag(self):
        self._query_test(
            [AndOp([[
                Tag(['a']),
                AuthorTag(['guy'])
            ]])],
            [self.posts.guy2]
        )

    def test_subject_tag(self):
        self._query_test(
            [SubjectTag(['first post'])],
            [self.posts.guy1, self.posts.lain1]
        )

        self._query_test(
            [SubjectTag(['a string not found anywhere'])],
            []
        )

    def test_negate_subject_tag(self):
        self._query_test(
            [NotOp([[
                SubjectTag(['first post'])
            ]])],
            [self.posts.guy2, self.posts.lain2, self.posts.no_author]
        )

    def test_mix_subject_tag(self):
        self._query_test(
            [AndOp([[
                Tag(['b']),
                SubjectTag(['first post'])
            ]])],
            [self.posts.guy1]
        )

    def test_description_tag(self):
        self._query_test(
            [DescriptionTag(['hello'])],
            [self.posts.guy1, self.posts.lain1]
        )

        self._query_test(
            [DescriptionTag(['a string not found anywhere'])],
            []
        )

    def test_negate_description_tag(self):
        self._query_test(
            [NotOp([[
                DescriptionTag(['hello'])
            ]])],
            [self.posts.guy2, self.posts.lain2, self.posts.no_author]
        )

    def test_mix_description_tag(self):
        self._query_test(
            [AndOp([[
                Tag(['b']),
                DescriptionTag(['hello'])
            ]])],
            [self.posts.guy1]
        )


class TestMediaTags(WithDB, QueryMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.posts = SimpleNamespace(
            none1=make_post(subject="no media"),
            none2=make_post(subject="same here"),
            image1=make_post(
                subject="an image",
                media=[make_media(mime_type=MIMEType.JPEG)]
            ),
            image2=make_post(
                subject="a few images",
                media=[
                    make_media(mime_type=MIMEType.JPEG),
                    make_media(mime_type=MIMEType.GIF),
                ]
            ),
            video1=make_post(
                subject="a video",
                media=[make_media(mime_type=MIMEType.WEBM)]
            ),
            video2=make_post(
                subject="a few videos",
                media=[
                    make_media(mime_type=MIMEType.WEBM),
                    make_media(mime_type=MIMEType.MPEG),
                ]
            ),
            image_video=make_post(
                subject="image and a video",
                media=[
                    make_media(mime_type=MIMEType.JPEG),
                    make_media(mime_type=MIMEType.WEBM),
                ]
            ),
            everything=make_post(
                subject="a bit of everything",
                media=[
                    make_media(mime_type=MIMEType.JPEG),
                    make_media(mime_type=MIMEType.WEBM),
                    make_media(mime_type=MIMEType.GIF),
                    make_media(mime_type=MIMEType.MPEG),
                    make_media(mime_type=MIMEType.ZIP),
                    make_media(mime_type=MIMEType.ODT),
                    make_media(mime_type=MIMEType.DOCX),
                    make_media(mime_type=MIMEType.SWF),
                ]
            )
        )

    def test_media_category_tag(self):
        self._query_test(
            [MediaCategoryTag(['none'])],
            [self.posts.none1, self.posts.none2]
        )

        self._query_test(
            [MediaCategoryTag(['image'])],
            [self.posts.image1, self.posts.image2, self.posts.image_video, self.posts.everything]
        )

        self._query_test(
            [MediaCategoryTag(['video'])],
            [self.posts.video1, self.posts.video2, self.posts.image_video, self.posts.everything]
        )

        self._query_test(
            [MediaCategoryTag(['document'])],
            [self.posts.everything]
        )

    def test_negate_media_category_tag(self):
        self._query_test(
            [NotOp([[
                MediaCategoryTag(['image'])
            ]])],
            [self.posts.none1, self.posts.none2, self.posts.video1, self.posts.video2]
        )

    def test_mix_media_category_tag(self):
        self._query_test(
            [AndOp([[
                MediaCategoryTag(['image']),
                MediaCategoryTag(['video']),
            ]])],
            [self.posts.image_video, self.posts.everything]
        )

    def test_media_category_tag_raises_on_unknown_type(self):
        with self.assertRaises(KeyError):
            self._query_test(
                [MediaCategoryTag(['not a category'])],
                []
            )

    def test_media_mimetype_tag(self):
        self._query_test(
            [MediaMIMETypeTag([MIMEType.GIF.value])],
            [self.posts.image2, self.posts.everything]
        )

        self._query_test(
            [MediaMIMETypeTag([MIMEType.MPEG.value])],
            [self.posts.video2, self.posts.everything]
        )

        self._query_test(
            [MediaMIMETypeTag([MIMEType.DOCX.value])],
            [self.posts.everything]
        )

    def test_negate_media_mimetype_tag(self):
        self._query_test(
            [NotOp([[
                MediaMIMETypeTag([MIMEType.JPEG.value])
            ]])],
            [self.posts.none1, self.posts.none2, self.posts.video1, self.posts.video2]
        )

    def test_mix_media_mimetype_tag(self):
        self._query_test(
            [AndOp([[
                MediaMIMETypeTag([MIMEType.JPEG.value]),
                MediaMIMETypeTag([MIMEType.WEBM.value]),
            ]])],
            [self.posts.image_video, self.posts.everything]
        )


class TestReplyTag(WithDB, QueryMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.parent1 = make_post()
        self.parent2 = make_post()
        self.reply1 = make_post(reply_to=self.parent1)
        self.reply2 = make_post(reply_to=self.parent1)
        self.reply_to_reply = make_post(reply_to=self.reply1)

    def test_reply_tag(self):
        self._query_test(
            [ReplyTag([self.parent1.id])],
            [self.reply1, self.reply2]
        )

        self._query_test(
            [ReplyTag([self.parent2.id])],
            []
        )

        self._query_test(
            [ReplyTag([self.reply1.id])],
            [self.reply_to_reply]
        )

        self._query_test(
            [ReplyTag(['false'])],
            [self.parent1, self.parent2]
        )

    def test_negate_reply_tag(self):
        self._query_test(
            [NotOp([[
                ReplyTag([self.parent1.id])
            ]])],
            [self.parent1, self.parent2, self.reply_to_reply]
        )
        self._query_test(
            [NotOp([[
                ReplyTag(['none'])
            ]])],
            [self.reply1, self.reply2, self.reply_to_reply]
        )
