"""Factories for test fixtures"""
from unittest import mock
from functools import wraps

from infosoc.db.models import User, Media, Post, Tag, Notification
from infosoc.db.mimetypes import MIMEType
from infosoc.app import db


def session_add(fn):
    """Decorator to automatically add fixtures to the current session"""
    @wraps(fn)
    def wrapper(*args, **kwargs):
        row = fn(*args, **kwargs)
        db.session.add(row)
        db.session.commit()
        return row
    return wrapper


user_defaults = dict(
    password_hash="not a real password",
    nickname="guy",
)


@session_add
def make_user(**kwargs):
    return User(**{**user_defaults, **kwargs})


media_defaults = dict(
    filename="original_filename.jpeg",
    mime_type=MIMEType.JPEG,
    path="path.jpg",
    md5sum="98a3851a4b9a4956839d54a6b57ca805"
)


@session_add
def make_media(**kwargs):
    return Media(**{**media_defaults, **kwargs})


file_store_defaults = dict(
    name="mock_form",
    filename="file name.jpeg",
    headers={
        'Content-Disposition': 'form-data; name="mock_form"; filename="file name.jpeg"',
        'Content-Type': 'image/jpeg'
    }
)


def make_file_store(**kwargs):
    return mock.MagicMock(**{**file_store_defaults, **kwargs})


post_defaults = dict()  # TODO


@session_add
def make_post(**kwargs):
    return Post(**{**post_defaults, **kwargs})


tag_defaults = dict(
    name="cool_tag"
)


@session_add
def make_tag(**kwargs):
    return Tag(**{**tag_defaults, **kwargs})


notification_defaults = dict()  # TODO


@session_add
def make_notification(**kwargs):
    return Notification(**{**notification_defaults, **kwargs})
