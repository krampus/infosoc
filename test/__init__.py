"""Shared unit test fixtures & functionality"""
import os
from pathlib import Path
os.environ["FLASK_APP"] = 'infosoc'
os.environ["FLASK_ENV"] = 'test'

test_conf_path = Path(__file__).parent / 'config.yaml'
os.environ["CONFIG_YAML"] = str(test_conf_path)

from infosoc.app import app, db as _db  # noqa: E402

DB_URI = "sqlite://"
SERVER_NAME = 'test.social'


def setup_package():
    app.config['TESTING'] = True
    app.config['DEBUG'] = False
    app.config['SERVER_NAME'] = SERVER_NAME


def teardown_package():
    pass


class WithDB:
    def setUp(self):
        super().setUp()
        with app.app_context():
            from infosoc.db import models  # noqa: F401
            assert str(_db.engine.url) == DB_URI
            _db.drop_all()
            _db.create_all()


class WithClient:
    def setUp(self):
        super().setUp()
        self.client = app.test_client()


class WithAppContext:
    def setUp(self):
        super().setUp()
        self._appctx = app.app_context()
        self._appctx.__enter__()

    def tearDown(self):
        self._appctx.__exit__(None, None, None)
