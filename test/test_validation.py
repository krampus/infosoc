"""Unit tests covering infosoc.validation"""
from unittest import mock
from unittest import TestCase

from infosoc import validation
from infosoc.db.mimetypes import MIMEType

from .fixture_builder import make_user, make_file_store
from . import WithDB, WithAppContext


class TestValidateNickname(WithDB, TestCase):
    def test_deny_empty_nickname(self):
        with self.assertRaises(AssertionError):
            validation.validate_nickname('')
        with self.assertRaises(AssertionError):
            validation.validate_nickname(None)

    def test_deny_used_nickname(self):
        make_user(nickname="guy")

        with self.assertRaises(AssertionError):
            validation.validate_nickname('guy')

    def test_deny_nickname_by_regex(self):
        with self.assertRaises(AssertionError):
            validation.validate_nickname('')
        with self.assertRaises(AssertionError):
            validation.validate_nickname('a' * 33)

        # this should pass though:
        validation.validate_nickname('guy')

    def test_deny_html_injection_in_nickname(self):
        with mock.patch.object(validation, '_nickname_pattern') as mock_pattern:
            # override regex filter (which would catch these otherwise)
            mock_pattern.fullmatch.return_value = True

            with self.assertRaises(AssertionError):
                validation.validate_nickname('guy<script>alert("nasty stuff!")</script>')

    def test_allow_valid_unused_nickname(self):
        make_user(nickname="guy")

        expected = "AnotherGuy2"
        self.assertEqual(expected, validation.validate_nickname(expected))


class TestValidatePassword(WithDB, TestCase):
    def test_deny_empty_password(self):
        with self.assertRaises(AssertionError):
            validation.validate_password('')
        with self.assertRaises(AssertionError):
            validation.validate_password(None)

    def test_deny_password_by_regex(self):
        with self.assertRaises(AssertionError):
            validation.validate_password('1234')

        # this should pass though:
        validation.validate_password('12345')

    def test_allow_valid_password(self):
        expected = "hunter2"
        self.assertEqual(expected, validation.validate_password(expected))


class TestValidateEmail(WithDB, TestCase):
    def setUp(self):
        super().setUp()

    def test_deny_empty_email(self):
        with self.assertRaises(AssertionError):
            validation.validate_email('')
        with self.assertRaises(AssertionError):
            validation.validate_email(None)

    def test_deny_malformed_email(self):
        with self.assertRaises(AssertionError):
            validation.validate_email('just.some.guy')
        with self.assertRaises(AssertionError):
            validation.validate_email('@guy@mastodon.social')

    def test_allow_valid_email(self):
        expected = "present.day@present.time"
        self.assertEqual(expected, validation.validate_email(expected))


class TestValidateText(WithDB, TestCase):
    def test_return_none_on_empty_text(self):
        self.assertIsNone(validation.validate_text(None))
        self.assertIsNone(validation.validate_text(''))

    def test_return_validated_text(self):
        expected = "Here's some ordinary text..."
        self.assertEqual(expected, validation.validate_text(expected))

        expected = "here's some text with [markdown](https://daringfireball.net/projects/markdown/)"
        self.assertEqual(expected, validation.validate_text(expected))

        expected = "here's some <i>text</i> with <b>allowed_html</b>"
        self.assertEqual(expected, validation.validate_text(expected))

    def test_bleach_bad_elements(self):
        text = "this text has <script>alert('malicious HTML')</script>"
        expected = "this text has &lt;script&gt;alert('malicious HTML')&lt;/script&gt;"
        self.assertEqual(expected, validation.validate_text(text))
        # TODO more comprehensive testing for this sort of thing


class TestValidateTag(WithDB, TestCase):
    def test_return_none_on_empty_tag(self):
        self.assertIsNone(validation.validate_tag(None))
        self.assertIsNone(validation.validate_tag(''))

    def test_deny_tag_by_regex(self):
        with self.assertRaises(AssertionError):
            validation.validate_tag('-bad-tag')
        with self.assertRaises(AssertionError):
            validation.validate_tag('a common error')
        with self.assertRaises(AssertionError):
            validation.validate_tag('invalid#characters')
        with self.assertRaises(AssertionError):
            validation.validate_tag('(bad_tag)')

        # this should pass though
        validation.validate_tag('still-the-best_1973')

    def test_deny_tag_by_special_key(self):
        with self.assertRaises(AssertionError):
            validation.validate_tag('sort:date')
        with self.assertRaises(AssertionError):
            validation.validate_tag('author:guy')
        with self.assertRaises(AssertionError):
            validation.validate_tag('subject:uhh')
        with self.assertRaises(AssertionError):
            validation.validate_tag('description:whatever')
        with self.assertRaises(AssertionError):
            validation.validate_tag('media:image')

        # this should still pass though
        validation.validate_tag('artist:lil_b')

    def test_allow_valid_tag(self):
        expected = 'still-the-best_1973'
        self.assertEqual(expected, validation.validate_tag(expected))


class TestValidateMedia(WithDB, WithAppContext, TestCase):
    def test_deny_unallowed_media_type(self):
        with self.assertRaises(AssertionError):
            validation.validate_media(
                make_file_store(headers={'Content-Type': MIMEType.SWF.value})
            )

    def test_allow_allowed_media_type(self):
        expected = make_file_store(headers={'Content-Type': MIMEType.GIF.value})
        self.assertEqual(expected, validation.validate_media(expected))

    def test_deny_invalid_image_type(self):
        with self.assertRaises(AssertionError):
            validation.validate_image(
                make_file_store(headers={'Content-Type': MIMEType.BMP.value})
            )

        with self.assertRaises(AssertionError):
            validation.validate_image(
                make_file_store(headers={'Content-Type': MIMEType.WEBM.value})
            )

    def test_allow_allowed_image_type(self):
        expected = make_file_store(headers={'Content-Type': MIMEType.GIF.value})
        self.assertEqual(expected, validation.validate_image(expected))

    def test_deny_invalid_video_type(self):
        with self.assertRaises(AssertionError):
            validation.validate_video(
                make_file_store(headers={'Content-Type': MIMEType.AVI.value})
            )

        with self.assertRaises(AssertionError):
            validation.validate_video(
                make_file_store(headers={'Content-Type': MIMEType.JPEG.value})
            )

    def test_allow_allowed_video_type(self):
        expected = make_file_store(headers={'Content-Type': MIMEType.WEBM.value})
        self.assertEqual(expected, validation.validate_video(expected))
