"""Unit tests covering database models"""
from unittest import TestCase

from infosoc.db.models import User, Media, Post, Tag, Notification, _post_tags, _user_likes
from infosoc.db.mimetypes import MIMEType
from infosoc.app import db

from ..fixture_builder import make_user, make_post, make_media, make_tag, make_notification
from .. import WithDB


class ModelBase(WithDB):
    def assertNoResults(self, query):
        self.assertEqual(
            query.count(), 0,
            msg=f"Expected no results for {query}, got {query.count()}"
        )

    def assert_user_deleted(self, user):
        self.assertNotIn(user, db.session)
        self.assertIsNone(db.session.query(User).get(user.id))
        self.assertNoResults(db.session.query(Post).filter_by(author_id=user.id))
        self.assertNoResults(db.session.query(_user_likes).filter_by(user_id=user.id))
        self.assertNoResults(db.session.query(Notification).filter_by(user_id=user.id))

    def assert_media_deleted(self, media):
        self.assertNotIn(media, db.session)
        self.assertIsNone(db.session.query(Media).get(media.id))
        self.assertNoResults(db.session.query(Post).filter(Post.media.any(id=media.id)))
        self.assertNoResults(db.session.query(Post).filter_by(thumbnail_id=media.id))
        self.assertNoResults(db.session.query(User).filter_by(avatar_id=media.id))

    def assert_post_deleted(self, post):
        self.assertNotIn(post, db.session)
        self.assertIsNone(db.session.query(Post).get(post.id))
        self.assertNoResults(db.session.query(Post).filter_by(reply_to_id=post.id))
        self.assertNoResults(db.session.query(Post).filter(Post.replies.any(id=post.id)))
        self.assertNoResults(db.session.query(Media).filter_by(post_id=post.id))
        self.assertNoResults(db.session.query(_post_tags).filter_by(post_id=post.id))
        self.assertNoResults(db.session.query(_user_likes).filter_by(post_id=post.id))

    def assert_tag_deleted(self, tag):
        self.assertNotIn(tag, db.session)
        self.assertIsNone(db.session.query(Tag).get(tag.id))
        self.assertNoResults(db.session.query(_post_tags).filter_by(tag_id=tag.id))

    def assert_notification_deleted(self, notification):
        self.assertNotIn(notification, db.session)
        self.assertIsNone(db.session.query(Notification).get(notification.id))
        self.assertNoResults(db.session.query(User).filter(User.notifications.any(id=notification.id)))


class TestUserModel(ModelBase, TestCase):
    def test_insert(self):
        user = User(nickname='guy', password_hash='')

        db.session.add(user)
        db.session.commit()

        self.assertIn(user, db.session)
        self.assertIsNotNone(user.id)
        self.assertEqual(user.nickname, 'guy')
        self.assertEqual(user.password_hash, '')
        self.assertIsNotNone(user.name)
        self.assertIsNotNone(user.following)
        self.assertTrue(user.local)

    def test_insert_cascades(self):
        avatar = Media(filename='file.jpg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')
        user = User(nickname='guy', password_hash='', avatar=avatar)

        db.session.add(user)
        db.session.commit()

        self.assertIn(user, db.session)
        self.assertIn(avatar, db.session)

    def test_delete_cascades(self):
        user_avatar = make_media()
        user = make_user(avatar=user_avatar)
        user_posts = [
            make_post(author=user, description="user's first post"),
            make_post(author=user, description="user's last post"),
        ]
        other = make_user(nickname='other')
        other_posts = [
            make_post(author=other, description="other's first post"),
            make_post(author=other, description="other's last post"),
        ]
        user_reply = make_post(author=user, reply_to=other_posts[0], description="reply to other's post")
        other_reply = make_post(author=other, reply_to=user_posts[0], description="reply to user's post")
        liked_posts = user_posts + other_posts
        user.liked_posts.extend(liked_posts)
        other.liked_posts.extend(liked_posts)

        user_notification = make_notification(user=user, content="notification for user")
        other_notification = make_notification(user=other, content="notification for other")

        db.session.commit()

        db.session.delete(user)
        db.session.commit()

        self.assert_user_deleted(user)
        self.assert_media_deleted(user_avatar)
        self.assert_notification_deleted(user_notification)

        for post in user_posts:
            self.assertNotIn(post, db.session)
            self.assertNotIn(post, other.liked_posts)
        self.assertNotIn(user_reply, db.session)
        self.assertNotIn(user_notification, db.session)

        self.assertIn(other, db.session)
        for post in other_posts:
            self.assertIn(other, db.session)
            self.assertEqual(len(post.replies), 0)
            self.assertListEqual(post.liked_by, [other])
            self.assertEqual(post.like_count, 1)

        self.assertIn(other_reply, db.session)
        self.assertIsNone(other_reply.reply_to)
        self.assertIn(other_notification, db.session)


class TestMediaModel(ModelBase, TestCase):
    def test_insert(self):
        media = Media(filename='media.jpeg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')

        db.session.add(media)
        db.session.commit()

        self.assertIn(media, db.session)
        self.assertIsNotNone(media.id)
        self.assertEqual(media.filename, 'media.jpeg')
        self.assertEqual(media.mime_type, MIMEType.JPEG)

    def test_delete_cascades(self):
        media = make_media()
        other = make_media()
        post = make_post(media=[media, other])
        thumbnail_post = make_post(thumbnail=media)
        user = make_user(avatar=media)

        db.session.commit()

        db.session.delete(media)
        db.session.commit()

        self.assert_media_deleted(media)

        self.assertIn(other, db.session)

        self.assertIn(post, db.session)
        self.assertEqual(post.media, [other])

        self.assertIn(thumbnail_post, db.session)
        self.assertIsNone(thumbnail_post.thumbnail)

        self.assertIn(user, db.session)
        self.assertIsNone(user.avatar)


class TestPostModel(ModelBase, TestCase):
    def test_insert(self):
        post = Post()

        db.session.add(post)
        db.session.commit()

        self.assertIn(post, db.session)
        self.assertIsNotNone(post.id)

    def test_insert_cascades(self):
        thumbnail = Media(filename='thumbnail.jpeg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')
        media = Media(filename='media.jpeg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')
        tag = Tag(name='tag')
        post = Post(thumbnail=thumbnail, media=[media], tags=[tag])

        db.session.add(post)
        db.session.commit()

        self.assertIn(post, db.session)
        self.assertIn(thumbnail, db.session)
        self.assertIn(media, db.session)
        self.assertIn(tag, db.session)

    def test_delete_cascades(self):
        user = make_user()
        parent_post = make_post(author=user, description="user's first post")

        thumbnail = Media(filename='thumbnail.jpeg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')
        media = Media(filename='media.jpeg', mime_type=MIMEType.JPEG, path='path.jpg', md5sum='')
        tag = Tag(name='tag')

        post = make_post(author=user, thumbnail=thumbnail, media=[media], tags=[tag],
                         liked_by=[user], reply_to=parent_post,
                         description="a reply to a post")

        child_post = make_post(author=user, reply_to=post,
                               description="a reply to a reply to a post")

        db.session.commit()

        db.session.delete(post)
        db.session.commit()

        self.assert_post_deleted(post)
        self.assert_media_deleted(thumbnail)
        self.assert_media_deleted(media)

        self.assertIn(user, db.session)
        self.assertNotIn(post, user.posts)
        self.assertNotIn(post, user.liked_posts)

        self.assertIn(parent_post, db.session)
        self.assertNotIn(post, parent_post.replies)

        self.assertIn(child_post, db.session)
        self.assertIsNone(child_post.reply_to)

        self.assertIn(tag, db.session)
        self.assertNotIn(post, tag.posts)
        self.assertEqual(tag.post_count, 0)


class TestTagModel(ModelBase, TestCase):
    def test_insert(self):
        tag = Tag(name='tag')

        db.session.add(tag)
        db.session.commit()

        self.assertIn(tag, db.session)
        self.assertIsNotNone(tag.id)
        self.assertEqual(tag.name, 'tag')

    def test_delete_cascades(self):
        tag = make_tag()
        other = make_tag(name='other')
        post = make_post(description='a post with tags', tags=[tag, other])

        db.session.commit()

        db.session.delete(tag)
        db.session.commit()

        self.assert_tag_deleted(tag)

        self.assertIn(post, db.session)
        self.assertNotIn(tag, post.tags)

        self.assertIn(other, db.session)
        self.assertIn(other, post.tags)


class TestNotificationModel(ModelBase, TestCase):
    def test_insert(self):
        notification = Notification()

        db.session.add(notification)
        db.session.commit()

        self.assertIn(notification, db.session)
        self.assertIsNotNone(notification.id)

    def test_delete_cascades(self):
        user = make_user()
        notification = make_notification(user=user, content="Let's all love lain!")
        other = make_notification(user=user, content="what?")

        db.session.commit()

        db.session.delete(notification)
        db.session.commit()

        self.assert_notification_deleted(notification)

        self.assertIn(other, db.session)

        self.assertIn(user, db.session)
        self.assertListEqual(user.notifications, [other])
