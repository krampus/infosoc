_This is a placeholder ToS. Instance administrators should replace the contents of this file with any applicable notices to be included in the instance's terms of service._

By using the service provided by this software, you agree to the following terms:

* While using this service you will be a cool guy.

* Your usage of this service comes with no warranty and no expectation of privacy or confidentiality.

Your user account may be terminated at any time, for any reason, including no reason at all.
